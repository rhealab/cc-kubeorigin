package cc.kubeorigin.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xzy on 2017/2/23.
 */
public class DateUtils {
    public final static String K8S_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static Date parseK8sDate(String k8sTimestampStr) {
        if (k8sTimestampStr == null) {
            return null;
        }
        Date date = null;
        try {
            date = new SimpleDateFormat(K8S_DATE_FORMAT).parse(k8sTimestampStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static long getAge(Date createdOn) {
        long age = 0;
        if (createdOn != null) {
            age = System.currentTimeMillis() - createdOn.getTime();
        }
        return age;
    }
}
