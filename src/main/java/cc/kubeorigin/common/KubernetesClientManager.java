package cc.kubeorigin.common;

import cc.kubeorigin.KubernetesProperties;
import cc.kubeorigin.common.utils.OkHttpClientUtils;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author wangchunyang@gmail.com
 * @maintained mrzihan.tang@gmail.com
 */

@Named
public class KubernetesClientManager {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManager.class);

    private KubernetesClient client;

    @Inject
    private KubernetesProperties properties;

    @PostConstruct
    private void init() {
        ConfigBuilder configBuilder = new ConfigBuilder()
                .withMasterUrl(properties.getMasterUrl())
                .withConnectionTimeout(Math.toIntExact(properties.getHttpConnectTimeoutMillis()))
                .withRequestTimeout(Math.toIntExact(properties.getHttpReadTimeoutMillis()));
        if (!properties.isLoginWithToken()) {
            configBuilder
                    .withUsername(properties.getAdmin().getUsername())
                    .withPassword(properties.getAdmin().getPassword())
                    .withTrustCerts(true);
        } else {
            configBuilder
                    .withOauthToken(properties.getToken())
                    .withTrustCerts(true);
        }

        Config config = configBuilder.build();

        logger.info("kubernetes.masterUrl={}", properties.getMasterUrl());

        client = new DefaultKubernetesClient(OkHttpClientUtils.createK8sHttpClient(config), config);
    }

    public KubernetesClient getClient() {
        return client;
    }
}
