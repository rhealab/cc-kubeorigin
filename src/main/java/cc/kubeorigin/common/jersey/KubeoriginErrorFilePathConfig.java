package cc.kubeorigin.common.jersey;

import cc.lib.retcode.config.ReturnCodeFilePathConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by xzy on 17-8-1.
 */
@Component
public class KubeoriginErrorFilePathConfig implements ReturnCodeFilePathConfigurer {
    @Override
    public void addFilePath(List<String> list) {
        list.add("error-code.json");
    }
}
