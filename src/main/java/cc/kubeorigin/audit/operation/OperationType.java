package cc.kubeorigin.audit.operation;

/**
 * Created by xzy on 2017/3/30.
 */
public enum OperationType {
    CREATE,
    DELETE,
    UPDATE,
    LOGIN,
    LOGOUT,
    RESTART
}
