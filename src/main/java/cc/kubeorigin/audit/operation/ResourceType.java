package cc.kubeorigin.audit.operation;

/**
 * Created by xzy on 2017,3,30.
 */
public enum ResourceType {
    NAMESPACE,
    USER,
    PROJECT,
    APPLICATION,
    STORAGE_CEPH_POOL,
    STORAGE_CEPH_USER,
    SECRET,
    STORAGE_CEPH_IMAGE,
    STORAGE_HOSTPATH,
    STORAGE_HOSTPATH_PV,
    STORAGE_HOSTPATH_PVC,
    ROLE_ASSIGNMENT,
    SERVICE("service"),
    DEPLOYMENT("deployment"),
    CPU,
    MEMORY,
    NODE,
    STORAGE_CEPHFS_PVC,
    STORAGE_CEPHFS_PV,
    STORAGE,
    STATEFULSET("statefulset"),
    REPLICATION_CONTROLLER("rc"),
    REPLICA_SET("rs"),
    ENDPOINTS("endpoints"),
    EVENT("event"),
    POD("pod");

    String name;

    ResourceType(){

    }
    ResourceType(String name){
        this.name=name;
    }

    public String getName(){
        return this.name;
    }

    public static ResourceType fromName(String name){
        for(ResourceType resourceType:ResourceType.values()){
            if(resourceType.getName()!=null && resourceType.getName().equals(name)){
                return resourceType;
            }
        }
        return null;
    }
}
