package cc.kubeorigin.audit.operation;

import cc.kubeorigin.audit.request.UserContext;
import com.google.gson.Gson;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/3/31.
 * @author wangchunyang@gmail.com
 */
public class OperationAuditMessage {
    private String requestId;
    private String userId;
    private String clientType;
    private String namespaceName;
    private String resourceName;
    private ResourceType resourceType;
    private OperationType operationType;
    private Date startTime;
    private long elapsed;
    private String extras;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public long getElapsed() {
        return elapsed;
    }

    public void setElapsed(long elapsed) {
        this.elapsed = elapsed;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    @Override
    public String toString() {
        return "OperationAuditMessage{" +
                "requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", clientType='" + clientType + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", resourceName='" + resourceName + '\'' +
                ", resourceType=" + resourceType +
                ", operationType=" + operationType +
                ", startTime=" + startTime +
                ", elapsed=" + elapsed +
                ", extras='" + extras + '\'' +
                '}';
    }

    public static class Builder {

        private String requestId;
        private String userId;
        private String clientType;
        private String namespaceName;
        private String resourceName;
        private ResourceType resourceType;
        private OperationType operationType;
        private Date startTime;
        private long elapsed;
        private Map<String, Object> extras = new HashMap<>();

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder clientType(String clientType) {
            this.clientType = clientType;
            return this;
        }

        public Builder namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder resourceName(String resourceName) {
            this.resourceName = resourceName;
            return this;
        }

        public Builder resourceType(ResourceType resourceType) {
            this.resourceType = resourceType;
            return this;
        }

        public Builder operationType(OperationType operationType) {
            this.operationType = operationType;
            return this;
        }

        public Builder startTime(Date startTime) {
            this.startTime = startTime;
            return this;
        }

        public Builder elapsed(Long elapsed) {
            if (elapsed != null && elapsed > 0) {
                this.elapsed = elapsed;
                this.startTime = Date.from(Instant.now().minus(elapsed, ChronoUnit.MILLIS));
            }
            return this;
        }

        public Builder extra(String key, Object value) {
            if (key != null && value != null) {
                this.extras.put(key, value);
            }
            return this;
        }

        public Builder extras(Map extras) {
            if (!extras.isEmpty()) {
                this.extras.putAll(extras);
            }
            return this;
        }

        public OperationAuditMessage build() {
            OperationAuditMessage m = new OperationAuditMessage();
            m.setRequestId(requestId != null ? requestId : UserContext.getRequestId());
            m.setUserId(userId != null ? userId : UserContext.getUserId());
            m.setClientType(clientType != null ? clientType : UserContext.getClientType());
            m.setNamespaceName(namespaceName != null ? namespaceName : UserContext.getNamespace());
            m.setResourceName(resourceName);
            m.setResourceType(resourceType);
            m.setOperationType(operationType);
            m.setStartTime(startTime);
            m.setElapsed(elapsed);
            m.setExtras(new Gson().toJson(extras));
            return m;
        }
    }
}