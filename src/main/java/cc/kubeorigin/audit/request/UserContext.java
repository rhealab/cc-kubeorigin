package cc.kubeorigin.audit.request;

import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangchunyang@gmail.com
 */
public class UserContext {
    private static ThreadLocal<ConcurrentHashMap<String, String>> threadLocal = ThreadLocal.withInitial(ConcurrentHashMap::new);

    public static void setRequestId(String requestId) {
        if (StringUtils.isNotBlank(requestId)) {
            threadLocal.get().put("requestId", requestId);
        }
    }

    public static String getRequestId() {
        return threadLocal.get().getOrDefault("requestId", "");
    }

    public static void setUserId(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            threadLocal.get().put("userId", userId);
        }
    }

    public static String getUserId() {
        return threadLocal.get().getOrDefault("userId", "");
    }

    public static void setClientType(String clientType) {
        if (!org.springframework.util.StringUtils.isEmpty(clientType)) {
            threadLocal.get().put("clientType", clientType);
        }
    }

    public static String getClientType() {
        return threadLocal.get().getOrDefault("clientType", Constants.CLIENT_TYPE_OTHERS);
    }

    public static void setNamespaceName(String namespaceName) {
        if (StringUtils.isNotBlank(namespaceName)) {
            threadLocal.get().put("namespaceName", namespaceName);
        }
    }

    public static String getNamespace() {
        return threadLocal.get().get("namespaceName");
    }
}
