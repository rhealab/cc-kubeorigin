package cc.kubeorigin.audit.request;

/**
 * @author wangchunyang@gmail.com
 */
public interface Constants {
    String REQ_ID = "x-gw-req-id";
    String USER_ID = "x-gw-user-id";
    String CLIENT_TYPE = "x-client-type";
    String CLIENT_TYPE_OTHERS = "others";
}
