package cc.kubeorigin;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangchunyang@gmail.com
 */
@Configuration
public class RabbitConfig {
    public static final String DEPLOYMENT_DELETED_Q = "deployment_deleted_q";

    public static final String OPERATION_AUDIT_Q = "operation_audit_q";
    public static final String REQUEST_AUDIT_Q = "request_audit_q";

    @Bean
    public Queue getDeploymentDeletedQueue() {
        return new Queue(DEPLOYMENT_DELETED_Q, true);
    }

    @Bean
    public Queue getOperationAuditQueue() {
        return new Queue(OPERATION_AUDIT_Q, true);
    }

    @Bean
    public Queue getRequestAuditQueue() {
        return new Queue(REQUEST_AUDIT_Q, true);
    }

    @Bean
    public MessageConverter getMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
