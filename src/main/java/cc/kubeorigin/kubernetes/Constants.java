package cc.kubeorigin.kubernetes;

/**
 * @author wangchunyang@gmail.com
 */
public interface Constants {
    String RESOURCE_NAME_PATTERN = "^[a-z0-9]([-a-z0-9]*[a-z0-9])?$";
    int RESOURCE_NAME_MAX = 100;

    String K_CPU = "cpu";
    String K_MEMORY = "memory";
    String K_STORAGE = "storage";

    String APP_LABEL = "X_APP";
}
