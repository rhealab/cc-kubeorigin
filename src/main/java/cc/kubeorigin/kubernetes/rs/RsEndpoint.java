package cc.kubeorigin.kubernetes.rs;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/replica_sets")
@Api(value = "ReplicaSet", description = "Operation about rs.", produces = "application/json")
public class RsEndpoint {
    @Inject
    private RsManagement rsManagement;

    @Inject
    private PodsManagement podsManagement;

    @GET
    @ApiOperation(value = "List rs in namespace.", responseContainer = "List", response = RsDto.class)
    public Response getRsList(@PathParam("namespaceName") String namespaceName) {
        List<ReplicaSet> rsList = rsManagement.getRsList(namespaceName);
        if (!rsList.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return Response.ok(RsDto.from(rsList, allPods)).build();
        }
        return Response.ok(new ArrayList<RsDto>()).build();
    }

    @POST
    @ApiOperation(value = "List rs belongs to given apps in namespace.", responseContainer = "List", response = RsDto.class)
    public Response getRsListOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        List<ReplicaSet> rsList = rsManagement.getRsOfApps(namespaceName, appNames);
        if (!rsList.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return Response.ok(RsDto.from(rsList, allPods)).build();
        }
        return Response.ok(new ArrayList<RsDto>()).build();
    }

    @GET
    @Path("{rsName}")
    @ApiOperation(value = "Get rs info.", response = RsDto.class)
    public Response getRs(@PathParam("namespaceName") String namespaceName,
                          @PathParam("rsName") String rsName) {
        ReplicaSet rs = rsManagement.getRs(namespaceName, rsName);
        if (rs != null) {
            List<Pod> pods = rsManagement.getRsPods(namespaceName, rs);
            return Response.ok(RsDto.from(rs, pods)).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.REPLICA_SET_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicaSet", rsName)).
                    build();
        }
    }

    @GET
    @Path("{rsName}/origin")
    @ApiOperation(value = "Get rs info of k8s original format.", response = ReplicaSet.class)
    public Response getRsOrigin(@PathParam("namespaceName") String namespaceName,
                                @PathParam("rsName") String rsName) {
        ReplicaSet rs = rsManagement.getRs(namespaceName, rsName);
        if (rs != null) {
            return Response.ok(rs).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.REPLICA_SET_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicaSet", rsName)).
                    build();
        }
    }

    @DELETE
    @Path("{rsName}")
    @ApiOperation(value = "Delete rs.")
    public Response deleteRs(@PathParam("namespaceName") String namespaceName,
                             @PathParam("rsName") String rsName) {
        boolean deleted = rsManagement.deleteRs(namespaceName, rsName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicaSet", rsName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.REPLICA_SET_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicaSet", rsName))
                    .build();
        }
    }

    @GET
    @Path("by_deployment/{deploymentName}/new")
    @ApiOperation(value = "Get newest rs info by deployment", response = RsDto.class)
    public Response getNewRsByDeployment(@PathParam("namespaceName") String namespaceName,
                                         @PathParam("deploymentName") String deploymentName) {
        ReplicaSet rs = rsManagement.getNewRsByDeployment(namespaceName, deploymentName);
        List<RsDto> dtos = new ArrayList<>();
        if (rs != null) {
            List<Pod> pods = rsManagement.getRsPods(namespaceName, rs);
            RsDto dto = RsDto.from(rs, pods);
            dtos.add(dto);
        }
        return Response.ok(dtos).build();
    }

    @PUT
    @ApiOperation(value = "Update rs by json", response = RsDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           ReplicaSet rs) {
        ReplicaSet result = rsManagement.update(namespaceName, rs);
        List<Pod> pods = podsManagement.getPodsByRs(namespaceName, result.getMetadata().getName());
        return Response.ok(RsDto.from(result, pods)).build();
    }
}
