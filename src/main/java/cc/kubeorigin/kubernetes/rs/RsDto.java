package cc.kubeorigin.kubernetes.rs;

import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.CommonUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSetSpec;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSetStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public class RsDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private LabelSelector selector;
    private Map<String, String> annotations;
    private Map<String, String> labels;
    private List<String> images;
    private PodInfo podInfo;
    private Date createdOn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "RsDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", annotations=" + annotations +
                ", labels=" + labels +
                ", images=" + images +
                ", podInfo=" + podInfo +
                ", createdOn=" + createdOn +
                '}';
    }

    /**
     * @param replicaSets not null
     * @param allPods     not null
     * @return composed rs list
     */
    public static List<RsDto> from(List<ReplicaSet> replicaSets, List<Pod> allPods) {
        List<RsDto> dtos = new ArrayList<>();

        for (ReplicaSet set : replicaSets) {
            //FIXME can not match Expressions
            Map<String, String> selector = set.getSpec().getSelector().getMatchLabels();
            List<Pod> pods = CommonUtils.filterPods(allPods, selector);
            dtos.add(from(set, pods));
        }

        return dtos;
    }

    /**
     * @param replicaSet not null
     * @param pods       not null
     * @return composed rs
     */
    public static RsDto from(ReplicaSet replicaSet, List<Pod> pods) {
        RsDto dto = new RsDto();

        ObjectMeta metadata = replicaSet.getMetadata();
        ReplicaSetSpec spec = replicaSet.getSpec();
        ReplicaSetStatus status = replicaSet.getStatus();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());

        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());

        List<String> images = new ArrayList<>();
        List<Container> containers = spec.getTemplate().getSpec().getContainers();
        for (Container container : containers) {
            images.add(container.getImage());
        }
        dto.setImages(images);

        dto.setPodInfo(CommonUtils.composePodInfo(spec.getReplicas(), status.getReplicas(), pods));

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));

        return dto;
    }
}
