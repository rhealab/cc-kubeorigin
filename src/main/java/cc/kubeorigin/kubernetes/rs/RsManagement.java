package cc.kubeorigin.kubernetes.rs;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Component
public class RsManagement {
    private static final Logger logger = LoggerFactory.getLogger(RsManagement.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private PodsManagement podsManagement;
    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<ReplicaSet> getRsList(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<ReplicaSet> replicaSets = client.extensions().replicaSets().inNamespace(namespaceName).list().getItems();
        return replicaSets == null ? new ArrayList<>() : replicaSets;
    }

    public List<ReplicaSet> getRsOfApps(String namespaceName, List<String> appNames) {
        return KubernetesUtils.getReplicaSetsOfDeployments(clientManager.getClient(), namespaceName,
                KubernetesUtils.getDeploymentsOfApps(clientManager.getClient(), namespaceName, appNames));
    }

    public ReplicaSet getRs(String namespaceName, String rsName) {
        KubernetesClient client = clientManager.getClient();
        return client.extensions().replicaSets().inNamespace(namespaceName).withName(rsName).get();
    }

    public boolean deleteRs(String namespaceName, String rsName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.extensions().replicaSets().inNamespace(namespaceName).withName(rsName).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.REPLICA_SET)
                .resourceName(rsName)
                .extra("deleted", deleted)
                .build();
        //TODO need to talk with junting
//        operationAuditMessageProducer.send(message);

        return deleted;
    }

    public List<Pod> getRsPods(String namespaceName, ReplicaSet replicaSet) {
        List<Pod> pods = new ArrayList<>();
        LabelSelector selector = replicaSet.getSpec().getSelector();
        if (selector != null) {
            //FIXME can not match Expressions
            pods = podsManagement.getPodsByLabels(namespaceName, selector.getMatchLabels());
        }

        return pods;
    }

    public ReplicaSet getNewRsByDeployment(String namespaceName, String deploymentName) {
        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName,deploymentName);


        PodTemplateSpec template = CommonUtils.getNewReplicaSetTemplate(deployment);

        List<ReplicaSet> rsList = getRsList(namespaceName);

        for (ReplicaSet replicaSet : rsList) {
            PodTemplateSpec rsTemplate = replicaSet.getSpec().getTemplate();
            if (CommonUtils.equalIgnoreHash(rsTemplate, template)) {
                return replicaSet;
            }
        }

        return null;
    }

    public ReplicaSet update(String namespaceName, ReplicaSet rs) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String rsName = rs.getMetadata().getName();
        ReplicaSet oldRs = resourceHelper.validateRsShouldExists(namespaceName, rsName);

//        resourceHelper.validateResourceIsOrigin(namespaceName, oldRs, ResourceType.REPLICA_SET);

//        resourceHelper.removeAppLabel(rs);

        KubernetesClient client = clientManager.getClient();
        rs = client.extensions().replicaSets().inNamespace(namespaceName).withName(rsName).replace(rs);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.REPLICATION_CONTROLLER)
                .resourceName(rsName)
                .extras(ImmutableMap.of("originName", rsName,
                        "currentName", rs.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return rs;
    }

}
