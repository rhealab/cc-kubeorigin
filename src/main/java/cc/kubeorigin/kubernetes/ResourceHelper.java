package cc.kubeorigin.kubernetes;


import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Map;

import static cc.kubeorigin.kubernetes.Constants.APP_LABEL;

/**
 * Created by xzy on 17-7-6.
 */
@Component
public class ResourceHelper {
    @Inject
    private KubernetesClientManager clientManager;

    public Deployment validateDeploymentShouldExists(String namespaceName, String deploymentName) {
        Deployment deployment = clientManager.getClient().extensions().deployments().inNamespace(namespaceName).withName(deploymentName).get();
        if (deployment == null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.DEPLOYMENT_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace", namespaceName,
                            "deployment", deploymentName));
        }
        return deployment;
    }

    public Endpoints validateEndpointsShouldExists(String namespaceName, String endpointsName) {
        Endpoints endpoints = clientManager.getClient().endpoints().inNamespace(namespaceName).withName(endpointsName).get();
        if (endpoints == null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.ENDPOINT_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace", namespaceName,
                            "endpoints", endpointsName));
        }
        return endpoints;
    }

    public Event validateEventShouldExists(String namespaceName, String eventName) {
        Event event = clientManager.getClient().events().inNamespace(namespaceName).withName(eventName).get();
        if (event == null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.EVENT_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace", namespaceName,
                            "event", event));
        }
        return event;
    }

    public Pod validatePodShouldExists(String namespace, String podName) {
        Pod pod = clientManager.getClient().pods().inNamespace(namespace).withName(podName).get();
        if (pod == null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.POD_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace", namespace,
                            "pod", podName));
        }
        return pod;
    }

    public ReplicationController validateRcShouldExists(String namespaceName,String rcName){
        ReplicationController rc=clientManager.getClient().replicationControllers().inNamespace(namespaceName).withName(rcName).get();
        if(rc==null){
            throw new CcException(KubeoriginReturnCodeNameConstants.REPLICATION_CONTROLLER_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace",namespaceName,
                            "replicationController",rcName));
        }
        return rc;
    }

    public ReplicaSet validateRsShouldExists(String namespaceName, String rsName){
        ReplicaSet rs =clientManager.getClient().extensions().replicaSets().inNamespace(namespaceName).withName(rsName).get();
        if(rs ==null){
            throw new CcException(KubeoriginReturnCodeNameConstants.REPLICA_SET_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace",namespaceName,
                            "replicaSet", rsName));
        }
        return rs;
    }

    public Service validateServiceShouldExists(String namespaceName,String serviceName){
        Service service =clientManager.getClient().services().inNamespace(namespaceName).withName(serviceName).get();
        if(service ==null){
            throw new CcException(KubeoriginReturnCodeNameConstants.SERVICE_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace",namespaceName,
                            "service", serviceName));
        }
        return service;
    }

    public StatefulSet validateStatefulsetShouldExists(String namespaceName,String statefulsetName){
        StatefulSet statefulset =clientManager.getClient().apps().statefulSets().inNamespace(namespaceName).withName(statefulsetName).get();
        if(statefulset ==null){
            throw new CcException(KubeoriginReturnCodeNameConstants.STATEFULSET_NOT_FOUND,
                    ImmutableMap.of(
                            "namespace",namespaceName,
                            "statefulset", statefulsetName));
        }
        return statefulset;
    }

    public void validateResourceIsOrigin(String namespaceName, HasMetadata resource, ResourceType resourceType) {
        Map<String, String> labels = resource.getMetadata().getLabels();
        if (labels != null && labels.get(APP_LABEL) != null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.RESOURCE_IS_NOT_ORIGIN,
                    ImmutableMap.of("resourceType", resourceType,
                            "namespace", namespaceName,
                            resourceType.getName(), resource.getMetadata().getName()));
        }
    }

    public void removeAppLabel(HasMetadata resource) {
        if (resource != null && resource.getMetadata() != null && resource.getMetadata().getLabels() != null) {
            resource.getMetadata().getLabels().remove(APP_LABEL);
        }
    }
}
