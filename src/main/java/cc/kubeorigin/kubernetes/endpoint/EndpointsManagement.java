package cc.kubeorigin.kubernetes.endpoint;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.service.ServiceManagement;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Endpoints;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.kubeorigin.audit.operation.ResourceType.ENDPOINTS;

/**
 * Created by xzy on 2017/2/23.
 */
@Component
public class EndpointsManagement {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private ServiceManagement serviceManagement;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<Endpoints> getEndpoints(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<Endpoints> endpointsList = client.endpoints().inNamespace(namespaceName).list().getItems();
        return endpointsList == null ? new ArrayList<>() : endpointsList;
    }

    public List<Endpoints> getEndpointsOfApps(String namespaceName, List<String> appNames) {
        return KubernetesUtils.getEndpointsOfApps(clientManager.getClient(), namespaceName, appNames);
    }

    public Endpoints getEndpoint(String namespaceName, String endpointName) {
        KubernetesClient client = clientManager.getClient();
        return client.endpoints().inNamespace(namespaceName).withName(endpointName).get();
    }

    public boolean deleteEndpoint(String namespaceName, String endpointName) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.endpoints().inNamespace(namespaceName).withName(endpointName).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.ENDPOINTS)
                .resourceName(endpointName)
                .extra("deleted", deleted)
                .build();
        //TODO need to talk with junting
//        operationAuditMessageProducer.send(message);

        return deleted;
    }

    //FIXME performance issue!!!
    public List<Endpoints> getEndpointByDeployment(String namespaceName, String deploymentName) {
        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName, deploymentName);

        List<Endpoints> endpointsList = new ArrayList<>();
        Map<String, String> labels = deployment.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            List<Service> services = CommonUtils.filterServices(serviceManagement.getServiceList(namespaceName), labels);
            for (Service service : services) {
                String name = service.getMetadata().getName();
                Endpoints endpoint = getEndpoint(namespaceName, name);
                if (endpoint != null) {
                    endpointsList.add(endpoint);
                }
            }
        }

        return endpointsList;
    }

    public List<Endpoints> getEndpointByRs(String namespaceName, String rsName) {
        ReplicaSet replicaSet = resourceHelper.validateRsShouldExists(namespaceName, rsName);

        List<Endpoints> endpointsList = new ArrayList<>();
        Map<String, String> labels = replicaSet.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            List<Service> services = CommonUtils.filterServices(serviceManagement.getServiceList(namespaceName), labels);
            for (Service service : services) {
                String name = service.getMetadata().getName();
                Endpoints endpoint = getEndpoint(namespaceName, name);
                if (endpoint != null) {
                    endpointsList.add(endpoint);
                }
            }
        }

        return endpointsList;
    }

    public List<Endpoints> getEndpointByRc(String namespaceName, String rcName) {
        ReplicationController replicationController = resourceHelper.validateRcShouldExists(namespaceName, rcName);

        List<Endpoints> endpointsList = new ArrayList<>();
        Map<String, String> labels = replicationController.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            List<Service> services = CommonUtils.filterServices(serviceManagement.getServiceList(namespaceName), labels);
            for (Service service : services) {
                String name = service.getMetadata().getName();
                Endpoints endpoint = getEndpoint(namespaceName, name);
                if (endpoint != null) {
                    endpointsList.add(endpoint);
                }
            }
        }

        return endpointsList;
    }

    public List<Endpoints> getEndpointByStatefulSet(String namespaceName, String statefulSetName) {
        StatefulSet statefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName, statefulSetName);

        List<Endpoints> endpointsList = new ArrayList<>();
        Map<String, String> labels = statefulSet.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            List<Service> serviceList = CommonUtils.filterServices(serviceManagement.getServiceList(namespaceName), labels);
            for (Service service : serviceList) {
                String name = service.getMetadata().getName();
                Endpoints endpoint = getEndpoint(namespaceName, name);
                if (endpoint != null) {
                    endpointsList.add(endpoint);
                }
            }
        }
        return endpointsList;
    }

    public Endpoints update(String namespaceName, Endpoints endpoints) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String endpointsName = endpoints.getMetadata().getName();
        Endpoints oldEndpoints = resourceHelper.validateEndpointsShouldExists(namespaceName, endpointsName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldEndpoints, ResourceType.ENDPOINTS);

        resourceHelper.removeAppLabel(endpoints);

        KubernetesClient client = clientManager.getClient();
        endpoints = client.endpoints().inNamespace(namespaceName).withName(endpointsName).replace(endpoints);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ENDPOINTS)
                .resourceName(endpointsName)
                .extras(ImmutableMap.of("originName", endpointsName,
                        "currentName", endpoints.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return endpoints;
    }

}
