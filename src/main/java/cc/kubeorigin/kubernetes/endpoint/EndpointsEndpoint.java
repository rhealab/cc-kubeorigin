package cc.kubeorigin.kubernetes.endpoint;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Endpoints;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 * @maintained mrzihan.tang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/endpoints")
@Api(value = "Endpoint", description = "Operation about endpoints.", produces = "application/json")
public class EndpointsEndpoint {
    @Inject
    private EndpointsManagement endpointsManagement;

    @GET
    @ApiOperation(value = "List endpoints in namespace.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointsList(@PathParam("namespaceName") String namespaceName) {
        List<Endpoints> endpoints = endpointsManagement.getEndpoints(namespaceName);
        return Response.ok(EndpointDto.from(endpoints)).build();
    }

    @POST
    @ApiOperation(value = "List endpoints belongs to given apps in namespace.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointsOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        List<Endpoints> endpoints = endpointsManagement.getEndpointsOfApps(namespaceName, appNames);
        return Response.ok(EndpointDto.from(endpoints)).build();
    }

    @GET
    @Path("{endpointName}")
    @ApiOperation(value = "Get endpoint info.", response = EndpointDto.class)
    public Response getEndpoint(@PathParam("namespaceName") String namespaceName,
                                @PathParam("endpointName") String endpointName) {
        Endpoints endpoint = endpointsManagement.getEndpoint(namespaceName, endpointName);
        if (endpoint != null) {
            return Response.ok(EndpointDto.from(endpoint)).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.ENDPOINT_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "endpoint", endpointName)).
                    build();
        }
    }

    @GET
    @Path("{endpointName}/origin")
    @ApiOperation(value = "Get endpoint info of k8s original format.", response = Endpoints.class)
    public Response getEndpointOrigin(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("endpointName") String endpointName) {
        Endpoints endpoint = endpointsManagement.getEndpoint(namespaceName, endpointName);
        if (endpoint != null) {
            return Response.ok(endpoint).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.ENDPOINT_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "endpoint", endpointName)).
                    build();
        }
    }

    @DELETE
    @Path("{endpointName}")
    @ApiOperation(value = "Delete endpoint.")
    public Response deleteEndpoint(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("endpointName") String endpointName) {
        boolean deleted = endpointsManagement.deleteEndpoint(namespaceName, endpointName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "endpoint", endpointName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.ENDPOINT_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "endpoint", endpointName))
                    .build();
        }
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get endpoint info by deployment.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointByDeployment(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("deploymentName") String deploymentName) {
        List<Endpoints> endpointsList = endpointsManagement.getEndpointByDeployment(namespaceName, deploymentName);
        return Response.ok(EndpointDto.from(endpointsList)).build();
    }

    @GET
    @Path("by_rc/{rcName}")
    @ApiOperation(value = "Get endpoint info by rc.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointByRc(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("rcName") String rcName) {
        List<Endpoints> endpointsList = endpointsManagement.getEndpointByRc(namespaceName, rcName);
        return Response.ok(EndpointDto.from(endpointsList)).build();
    }

    @GET
    @Path("by_rs/{rsName}")
    @ApiOperation(value = "Get endpoint info by rs.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointByRs(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("rsName") String rsName) {
        List<Endpoints> endpointsList = endpointsManagement.getEndpointByRs(namespaceName, rsName);
        return Response.ok(EndpointDto.from(endpointsList)).build();
    }

    @GET
    @Path("by_statefulset/{statefulsetName}")
    @ApiOperation(value = "Get endpoint info by statefulset.", responseContainer = "List", response = EndpointDto.class)
    public Response getEndpointByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                             @PathParam("statefulsetName") String statefulsetName) {
        List<Endpoints> endpointsList = endpointsManagement.getEndpointByStatefulSet(namespaceName, statefulsetName);
        return Response.ok(EndpointDto.from(endpointsList)).build();
    }

    @PUT
    @ApiOperation(value = "Update endpoint by json.", response = EndpointDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           Endpoints endpoints) {
        Endpoints result = endpointsManagement.update(namespaceName, endpoints);
        return Response.ok(EndpointDto.from(result)).build();
    }
}
