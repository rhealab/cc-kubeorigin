package cc.kubeorigin.kubernetes.endpoint;

import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.Kind;
import io.fabric8.kubernetes.api.model.EndpointSubset;
import io.fabric8.kubernetes.api.model.Endpoints;
import io.fabric8.kubernetes.api.model.ObjectMeta;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public class EndpointDto {
    private String name;
    private String uid;
    private String namespaceName;
    private Map<String, String> labels;
    private Map<String, String> annotations;
    private Kind kind;
    private Date createdOn;
    private List<EndpointSubset> subset;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<EndpointSubset> getSubset() {
        return subset;
    }

    public void setSubset(List<EndpointSubset> subset) {
        this.subset = subset;
    }

    @Override
    public String toString() {
        return "EndpointDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", kind=" + kind +
                ", createdOn=" + createdOn +
                ", subset=" + subset +
                '}';
    }

    /**
     * @param endpoints not null
     * @return composed endpoint list
     */
    public static List<EndpointDto> from(List<Endpoints> endpoints) {
        return endpoints.stream()
                .map(EndpointDto::from)
                .collect(Collectors.toList());
    }

    /**
     * @param endpoint not null
     * @return composed endpoint
     */
    public static EndpointDto from(Endpoints endpoint) {
        EndpointDto dto = new EndpointDto();

        ObjectMeta metadata = endpoint.getMetadata();
        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespaceName(metadata.getNamespace());
        dto.setLabels(metadata.getLabels());
        dto.setAnnotations(metadata.getAnnotations());
        dto.setKind(Kind.Endpoint);
        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setSubset(endpoint.getSubsets());

        return dto;
    }
}
