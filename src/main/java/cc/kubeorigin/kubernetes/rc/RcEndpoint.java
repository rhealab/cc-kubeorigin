package cc.kubeorigin.kubernetes.rc;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/replication_controllers")
@Api(value = "ReplicationController", description = "Operation about rc.", produces = "application/json")
public class RcEndpoint {
    @Inject
    private RcManagement rcManagement;

    @Inject
    private PodsManagement podsManagement;

    @GET
    @ApiOperation(value = "List rc in namespace.", responseContainer = "List", response = RcDto.class)
    public Response getRcList(@PathParam("namespaceName") String namespaceName) {
        List<ReplicationController> rcList = rcManagement.getRcList(namespaceName);
        if (!rcList.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return Response.ok(RcDto.from(rcList, allPods)).build();
        }
        return Response.ok(new ArrayList<RcDto>()).build();
    }

    @POST
    @ApiOperation(value = "List rc in namespace which belong to given apps.", responseContainer = "List", response = RcDto.class)
    public Response getReplicationControllersOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        List<ReplicationController> rcList = rcManagement.getReplicationControllersOfApps(namespaceName, appNames);
        if (!rcList.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return Response.ok(RcDto.from(rcList, allPods)).build();
        }
        return Response.ok(new ArrayList<RcDto>()).build();
    }

    @GET
    @Path("{rcName}")
    @ApiOperation(value = "Get rc info.", response = RcDto.class)
    public Response getRc(@PathParam("namespaceName") String namespaceName,
                          @PathParam("rcName") String rcName) {
        ReplicationController rc = rcManagement.getRc(namespaceName, rcName);
        if (rc != null) {
            List<Pod> pods = podsManagement.getPodsByRc(namespaceName, rcName);
            return Response.ok(RcDto.from(rc, pods)).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.REPLICATION_CONTROLLER_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicationController", rcName)).
                    build();
        }
    }

    @GET
    @Path("{rcName}/origin")
    @ApiOperation(value = "Get rc info of k8s original format.", response = ReplicationController.class)
    public Response getRcOrigin(@PathParam("namespaceName") String namespaceName,
                                @PathParam("rcName") String rcName) {
        ReplicationController rc = rcManagement.getRc(namespaceName, rcName);
        if (rc != null) {
            return Response.ok(rc).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.REPLICATION_CONTROLLER_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicationController", rcName)).
                    build();
        }
    }


    @DELETE
    @Path("{rcName}")
    @ApiOperation(value = "Delete rc.")
    public Response deleteRc(@PathParam("namespaceName") String namespaceName,
                             @PathParam("rcName") String rcName) {
        boolean deleted = rcManagement.deleteRc(namespaceName, rcName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicationController", rcName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.REPLICATION_CONTROLLER_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "replicationController", rcName))
                    .build();
        }
    }

    @PUT
    @ApiOperation(value = "Update rc by json.", response = RcDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           ReplicationController rc) {
        ReplicationController result = rcManagement.update(namespaceName, rc);
        List<Pod> pods = podsManagement.getPodsByRc(namespaceName, result.getMetadata().getName());
        return Response.ok(RcDto.from(result, pods)).build();
    }
}
