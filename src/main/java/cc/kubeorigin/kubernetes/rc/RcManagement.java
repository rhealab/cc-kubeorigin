package cc.kubeorigin.kubernetes.rc;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Component
public class RcManagement {
    private static final Logger logger = LoggerFactory.getLogger(RcManagement.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<ReplicationController> getRcList(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<ReplicationController> rcList = client.replicationControllers().inNamespace(namespaceName).list().getItems();
        return rcList == null ? new ArrayList<>() : rcList;
    }

    public List<ReplicationController> getReplicationControllersOfApps(String namespaceName, List<String> appNames) {
        return KubernetesUtils.getReplicationControllersOfApps(clientManager.getClient(), namespaceName, appNames);
    }

    public ReplicationController getRc(String namespaceName, String rcName) {
        KubernetesClient client = clientManager.getClient();
        return client.replicationControllers().inNamespace(namespaceName).withName(rcName).get();
    }

    public boolean deleteRc(String namespaceName, String rcName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.replicationControllers().inNamespace(namespaceName).withName(rcName).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.REPLICATION_CONTROLLER)
                .resourceName(rcName)
                .extra("deleted", deleted)
                .build();
        //TODO need to talk with junting
//        operationAuditMessageProducer.send(message);

        return deleted;
    }

    public ReplicationController update(String namespaceName, ReplicationController rc) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String rcName = rc.getMetadata().getName();
        ReplicationController oldRc = resourceHelper.validateRcShouldExists(namespaceName,rcName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldRc, ResourceType.REPLICATION_CONTROLLER);

        resourceHelper.removeAppLabel(rc);

        KubernetesClient client = clientManager.getClient();
        rc = client.replicationControllers().inNamespace(namespaceName).withName(rcName).replace(rc);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.REPLICATION_CONTROLLER)
                .resourceName(rcName)
                .extras(ImmutableMap.of("originName", rcName,
                        "currentName", rc.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return rc;
    }

}
