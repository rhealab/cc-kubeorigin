package cc.kubeorigin.kubernetes.rc;

import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.rs.PodInfo;
import io.fabric8.kubernetes.api.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public class RcDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private Map<String, String> selector;
    private Map<String, String> annotations;
    private Map<String, String> labels;
    private List<String> images;
    private PodInfo podInfo;
    private Date createdOn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "RcDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", annotations=" + annotations +
                ", labels=" + labels +
                ", images=" + images +
                ", podInfo=" + podInfo +
                ", createdOn=" + createdOn +
                '}';
    }

    /**
     * @param replicationControllers not null
     * @param allPods                not null
     * @return composed rc
     */
    public static List<RcDto> from(List<ReplicationController> replicationControllers, List<Pod> allPods) {
        List<RcDto> dtos = new ArrayList<>();

        for (ReplicationController controller : replicationControllers) {
            Map<String, String> selector = controller.getSpec().getSelector();
            List<Pod> pods = CommonUtils.filterPods(allPods, selector);
            dtos.add(from(controller, pods));
        }

        return dtos;
    }

    /**
     * @param replicationController not null
     * @param pods                  not null
     * @return composed rc
     */
    public static RcDto from(ReplicationController replicationController, List<Pod> pods) {
        RcDto dto = new RcDto();

        ObjectMeta metadata = replicationController.getMetadata();
        ReplicationControllerSpec spec = replicationController.getSpec();
        ReplicationControllerStatus status = replicationController.getStatus();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());

        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());
        dto.setAnnotations(metadata.getAnnotations());

        List<String> images = new ArrayList<>();
        List<Container> containers = spec.getTemplate().getSpec().getContainers();
        for (Container container : containers) {
            images.add(container.getImage());
        }
        dto.setImages(images);

        PodInfo podInfo = CommonUtils.composePodInfo(spec.getReplicas(), status.getReplicas(), pods);
        dto.setPodInfo(podInfo);

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));

        return dto;
    }
}
