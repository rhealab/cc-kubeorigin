package cc.kubeorigin.kubernetes.utils;

import io.fabric8.kubernetes.api.model.Endpoints;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by xzy on 18-4-17.
 */
public class KubernetesUtils {
  public static final String X_APP = "X_APP";

  public static List<Deployment> getDeploymentsOfApps(KubernetesClient client, String namespaceName, List<String> appNames) {
    if (appNames == null) {
      return new ArrayList<>();
    }
    List<Deployment> deployments = client.extensions().deployments().inNamespace(namespaceName).list().getItems();

    return Optional.ofNullable(deployments).orElseGet(ArrayList::new)
            .stream()
            .filter(deploy -> appNames.contains(Optional.ofNullable(deploy.getMetadata().getLabels()).orElseGet(HashMap::new).get(X_APP)))
            .collect(Collectors.toList());
  }

  public static List<StatefulSet> getStatefulSetsOfApps(KubernetesClient client, String namespaceName, List<String> appNames) {
    if (appNames == null) {
      return new ArrayList<>();
    }
    List<StatefulSet> statefulSets = client.apps().statefulSets().inNamespace(namespaceName).list().getItems();

    return Optional.ofNullable(statefulSets).orElseGet(ArrayList::new)
            .stream()
            .filter(statefulSet -> appNames.contains(Optional.ofNullable(statefulSet.getMetadata().getLabels()).orElseGet(HashMap::new).get(X_APP)))
            .collect(Collectors.toList());
  }

  public static List<ReplicationController> getReplicationControllersOfApps(KubernetesClient client, String namespaceName, List<String> appNames) {
    if (appNames == null) {
      return new ArrayList<>();
    }
    List<ReplicationController> replicationControllers = client.replicationControllers().inNamespace(namespaceName).list().getItems();

    return Optional.ofNullable(replicationControllers).orElseGet(ArrayList::new)
            .stream()
            .filter(replicationController -> appNames.contains(Optional.ofNullable(replicationController.getMetadata().getLabels()).orElseGet(HashMap::new).get(X_APP)))
            .collect(Collectors.toList());
  }

  public static List<Service> getServicesOfApps(KubernetesClient client, String namespaceName, List<String> appNames) {
    if (appNames == null) {
      return new ArrayList<>();
    }
    List<Service> svcs = client.services().inNamespace(namespaceName).list().getItems();
    return Optional.ofNullable(svcs).orElseGet(ArrayList::new)
            .stream()
            .filter(service -> appNames.contains(Optional.ofNullable(service.getMetadata().getLabels()).orElseGet(HashMap::new).get(X_APP)))
            .collect(Collectors.toList());
  }

  public static List<Endpoints> getEndpointsOfApps(KubernetesClient client, String namespaceName, List<String> appNames) {
    if (appNames == null) {
      return new ArrayList<>();
    }
    List<Endpoints> eps = client.endpoints().inNamespace(namespaceName).list().getItems();
    return Optional.ofNullable(eps).orElseGet(ArrayList::new)
            .stream()
            .filter(service -> appNames.contains(Optional.ofNullable(service.getMetadata().getLabels()).orElseGet(HashMap::new).get(X_APP)))
            .collect(Collectors.toList());
  }

  public static List<ReplicaSet> getReplicaSetsOfDeployments(KubernetesClient client,
                                                             String namespaceName, List<Deployment> deployments) {
    Set<String> deploymentNames = Optional.ofNullable(deployments).orElseGet(ArrayList::new)
            .stream()
            .collect(HashSet::new,
            (set, deployment)->set.add(deployment.getMetadata().getName()),
            (l1, l2)->l1.addAll(l2));

    List<ReplicaSet> replicaSets = client.extensions().replicaSets().inNamespace(namespaceName).list().getItems();

    return Optional.ofNullable(replicaSets).orElseGet(ArrayList::new)
            .stream().filter(rs->
              Optional.ofNullable(rs.getMetadata().getOwnerReferences()).orElseGet(ArrayList::new)
                      .stream()
                      .filter(owner -> deploymentNames.contains(owner.getName()) && "Deployment".equals(owner.getKind())).collect(Collectors.toList()).size() > 0
            ).collect(Collectors.toList());
  }

  public static List<Pod> getPodsOfReplicaSet(KubernetesClient client,
                                                      String namespaceName, List<ReplicaSet> replicaSets) {
    Set<String> rsNames = Optional.ofNullable(replicaSets).orElseGet(ArrayList::new)
            .stream()
            .collect(HashSet::new,
                    (set, deployment)->set.add(deployment.getMetadata().getName()),
                    (s1, s2)->s1.addAll(s2));

    List<Pod> pods = client.pods().inNamespace(namespaceName).list().getItems();

    return Optional.ofNullable(pods).orElseGet(ArrayList::new)
            .stream().filter(pod->
                    Optional.ofNullable(pod.getMetadata().getOwnerReferences()).orElseGet(ArrayList::new)
                            .stream()
                            .filter(owner -> rsNames.contains(owner.getName()) && "ReplicaSet".equals(owner.getKind())).collect(Collectors.toList()).size() > 0
            ).collect(Collectors.toList());
  }

  /**
   *
   * @param resources can not be null
   * @param controllers can not be null
   * @param controllerKind
   * @return
   */
  public static List<HasMetadata> getResourcesByControllers(List<? extends HasMetadata> resources,
                                                            List<? extends HasMetadata> controllers, String controllerKind) {
    Set<String> controllersName = controllers.stream()
            .collect(HashSet::new, (set, controller)->set.add(controller.getMetadata().getName()), (s1, s2)->s1.addAll(s2));

    return resources.stream().filter(r ->
            Optional.ofNullable(r.getMetadata().getOwnerReferences()).orElseGet(ArrayList::new)
                    .stream()
                    .filter(owner -> controllers.contains(owner.getName()) && controllerKind.equals(owner.getKind()))
                    .collect(Collectors.toList()).size() > 0
    ).collect(Collectors.toList());
  }
}
