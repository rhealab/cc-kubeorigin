package cc.kubeorigin.kubernetes;

import cc.kubeorigin.kubernetes.rs.PodInfo;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
public class CommonUtils {

    public static List<Pod> filterPods(List<Pod> pods, Map<String, String> selector) {
        List<Pod> filteredPods = new ArrayList<>();
        for (Pod pod : pods) {
            Map<String, String> labels = pod.getMetadata().getLabels();

            boolean isContained = true;
            for (Map.Entry<String, String> entry : selector.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (!(labels.get(key) == null && labels.containsKey(key))) {
                        isContained = false;
                        break;
                    }
                } else {
                    if (!equals(value, labels.get(key))) {
                        isContained = false;
                        break;
                    }
                }
            }

            if (isContained) {
                filteredPods.add(pod);
            }
        }
        return filteredPods;
    }

    public static boolean equals(String s1, String s2) {
        return s1 == s2 || s1 != null && s1.equals(s2);
    }

    public static List<Service> filterServices(List<Service> services, Map<String, String> labels) {
        List<Service> filteredServices = new ArrayList<>();

        for (Service service : services) {
            Map<String, String> serviceSelector = service.getSpec().getSelector();

            if (serviceSelector == null || labels.size() < serviceSelector.size()) {
                continue;
            }

            boolean isContained = true;

            for (Map.Entry<String, String> entry : serviceSelector.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (!(labels.get(key) == null && labels.containsKey(key))) {
                        isContained = false;
                        break;
                    }
                } else {
                    if (!value.equals(labels.get(key))) {
                        isContained = false;
                        break;
                    }
                }
            }

            if (isContained) {
                filteredServices.add(service);
            }
        }

        return filteredServices;
    }

    public static PodInfo composePodInfo(Integer desired, Integer current, List<Pod> pods) {
        PodInfo podInfo = new PodInfo();

        podInfo.setDesired(desired == null ? 0 : desired);
        podInfo.setCurrent(current == null ? 0 : current);

        int running = 0;
        int pending = 0;
        int failed = 0;
        int succeeded = 0;

        for (Pod pod : pods) {
            switch (pod.getStatus().getPhase()) {
                case "Running":
                    running++;
                    break;
                case "Pending":
                    pending++;
                    break;
                case "Failed":
                    failed++;
                    break;
                case "Succeeded":
                    succeeded++;
                    break;
            }
        }

        podInfo.setPending(pending);
        podInfo.setRunning(running);
        podInfo.setFailed(failed);
        podInfo.setSucceeded(succeeded);

        return podInfo;
    }

    public static PodTemplateSpec getNewReplicaSetTemplate(Deployment deployment) {
        PodTemplateSpec templateSpec = new PodTemplateSpec();

        templateSpec.setMetadata(deployment.getSpec().getTemplate().getMetadata());
        templateSpec.setSpec(deployment.getSpec().getTemplate().getSpec());

        return templateSpec;
    }

    public static boolean equalIgnoreHash(PodTemplateSpec template1, PodTemplateSpec template2) {
        ObjectMeta metadata1 = template1.getMetadata();
        ObjectMeta metadata2 = template2.getMetadata();
        Map<String, String> labels1 = metadata1.getLabels();
        Map<String, String> labels2 = metadata2.getLabels();

        if (labels1 == null) {
            labels1 = new HashMap<>();
        }
        if (labels2 == null) {
            labels2 = new HashMap<>();
        }

        if (labels1.size() > labels2.size()) {
            Map<String, String> temp = labels1;
            labels1 = labels2;
            labels2 = temp;
        }

        for (Map.Entry<String, String> entry : labels2.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (value == null) {
                if (labels1.get(key) != null && !"pod-template-hash".equals(key)) {
                    return false;
                }
            } else if (!value.equals(labels1.get(key)) && !"pod-template-hash".equals(key)) {
                return false;
            }
        }

        labels1 = metadata1.getLabels();
        labels2 = metadata2.getLabels();

        metadata1.setLabels(null);
        metadata2.setLabels(null);

        boolean equals = template1.equals(template2);

        metadata1.setLabels(labels1);
        metadata2.setLabels(labels2);

        return equals;
    }
}
