package cc.kubeorigin.kubernetes.deployment;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/deployments")
@Api(value = "Deployment", description = "Operation about deployments.", produces = "application/json")
public class DeploymentEndpoint {
    @Inject
    private DeploymentManagement deploymentManagement;

    @Inject
    private PodsManagement podsManagement;

    @GET
    @ApiOperation(value = "List deployments in namespace.", responseContainer = "List", response = DeploymentDto.class)
    public Response getDeploymentList(@PathParam("namespaceName") String namespaceName) {
        List<Deployment> deployments = deploymentManagement.getDeploymentList(namespaceName);
        if (!deployments.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return Response.ok(DeploymentDto.from(deployments, allPods)).build();
        }
        return Response.ok(new ArrayList<DeploymentDto>()).build();
    }

    @POST
    //@Path("/apps")
    @ApiOperation(value = "List deployments in namespace belong to given apps.", responseContainer = "List", response = DeploymentDto.class)
    public Response getDeploymentsOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        return Response.ok(deploymentManagement.getDeploymentsOfApps(namespaceName, appNames)).build();
    }

    @GET
    @Path("origin_list")
    @ApiOperation(value = "List deployments in namespace with k8s original form", responseContainer = "List", response = Deployment.class)
    public Response getDeploymentListOrigin(@PathParam("namespaceName") String namespaceName) {
        List<Deployment> deployments = deploymentManagement.getDeploymentList(namespaceName);
        return Response.ok(deployments).build();
    }

    @GET
    @Path("{deploymentName}")
    @ApiOperation(value = "Get deployment info.", response = DeploymentDto.class)
    public Response getDeployment(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("deploymentName") String deploymentName) {
        Deployment deployment = deploymentManagement.getDeployment(namespaceName, deploymentName);
        if (deployment != null) {
            List<Pod> pods = deploymentManagement.getDeploymentPods(namespaceName, deployment);
            return Response.ok(DeploymentDto.from(deployment, pods)).build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.DEPLOYMENT_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "deployment", deploymentName))
                    .build();
        }
    }

    @GET
    @Path("{deploymentName}/origin")
    @ApiOperation(value = "Get deployment info of k8s original format.", response = Deployment.class)
    public Response getDeploymentOrigin(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("deploymentName") String deploymentName) {
        Deployment deployment = deploymentManagement.getDeployment(namespaceName, deploymentName);
        if (deployment != null) {
            return Response.ok(deployment).build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.DEPLOYMENT_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "deployment", deploymentName))
                    .build();
        }
    }

    @PUT
    @ApiOperation(value = "Update deployment by json.", response = DeploymentDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           Deployment deployment) {
        Deployment result = deploymentManagement.update(namespaceName, deployment);
        List<Pod> pods = deploymentManagement.getDeploymentPods(namespaceName, result);
        return Response.ok(DeploymentDto.from(result, pods)).build();
    }

    @DELETE
    @Path("{deploymentName}")
    @ApiOperation(value = "Delete deployment.")
    public Response deleteDeployment(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("deploymentName") String deploymentName) {
        boolean deleted = deploymentManagement.deleteDeployment(namespaceName, deploymentName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "deployment", deploymentName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.DEPLOYMENT_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "deployment", deploymentName))
                    .build();
        }
    }

    @PUT
    @Path("restart/{deploymentName}")
    @ApiOperation(value = "restart deployment.")
    public Response restartDeployment(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("deploymentName") String deploymentName) {
        deploymentManagement.restart(namespaceName, deploymentName);
        return Response.ok().build();
    }
}
