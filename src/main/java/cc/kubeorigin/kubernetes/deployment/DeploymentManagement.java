package cc.kubeorigin.kubernetes.deployment;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.kubeorigin.audit.operation.ResourceType.DEPLOYMENT;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
@Component
public class DeploymentManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentManagement.class);
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<Deployment> getDeploymentList(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<Deployment> deployments = client.extensions().
                deployments().inNamespace(namespaceName).list().getItems();
        return deployments == null ? new ArrayList<>() : deployments;
    }

    public List<DeploymentDto> getDeploymentsOfApps(String namespaceName, List<String> appNames) {
        List<Deployment> wanted = KubernetesUtils.getDeploymentsOfApps(clientManager.getClient(), namespaceName, appNames);
        if (!wanted.isEmpty()) {
            List<Pod> allPods = podsManagement.getPods(namespaceName);
            return DeploymentDto.from(wanted, allPods);
        }
        return new ArrayList<>();
    }

    public Deployment getDeployment(String namespaceName, String deploymentName) {
        KubernetesClient client = clientManager.getClient();
        return client.extensions().deployments().inNamespace(namespaceName).withName(deploymentName).get();
    }

    public List<Pod> getDeploymentPods(String namespaceName, Deployment deployment) {
        List<Pod> pods = new ArrayList<>();
        LabelSelector selector = deployment.getSpec().getSelector();
        if (selector != null) {
            //FIXME can not match Expressions
            pods = podsManagement.getPodsByLabels(namespaceName, selector.getMatchLabels());
        }

        return pods;
    }

    public boolean deleteDeployment(String namespaceName, String deploymentName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName,deploymentName);

        resourceHelper.validateResourceIsOrigin(namespaceName, deployment, ResourceType.DEPLOYMENT);

        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.extensions().deployments().inNamespace(namespaceName).withName(deploymentName).delete();
        sendDeploymentDeletedMessage(namespaceName, deploymentName);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(DEPLOYMENT)
                .resourceName(deploymentName)
                .extra("deleted", deleted)
                .build();
        operationAuditMessageProducer.send(message);

        return deleted;
    }

    private void sendDeploymentDeletedMessage(String namespaceName, String deploymentName) {

    }

    public Deployment update(String namespaceName, Deployment deployment) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String deploymentName = deployment.getMetadata().getName();
        Deployment oldDeployment = resourceHelper.validateDeploymentShouldExists(namespaceName,deploymentName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldDeployment, ResourceType.DEPLOYMENT);

        resourceHelper.removeAppLabel(deployment);

        KubernetesClient client = clientManager.getClient();
        deployment = client.extensions().deployments().inNamespace(namespaceName).withName(deploymentName).replace(deployment);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(DEPLOYMENT)
                .resourceName(deploymentName)
                .extras(ImmutableMap.of("originName", deploymentName,
                        "currentName", deployment.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return deployment;
    }

    public void restart(String namespaceName, String deploymentName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<Pod> pods = podsManagement.getPodsByDeployment(namespaceName, deploymentName);

        pods.stream().forEach(pod ->
            podsManagement.deletePod(namespaceName, pod.getMetadata().getName(), -1L)
        );

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.RESTART)
                .resourceType(DEPLOYMENT)
                .resourceName(deploymentName)
                .build();
        operationAuditMessageProducer.send(message);
    }
}
