package cc.kubeorigin.kubernetes.deployment;

import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerPort;
import io.fabric8.kubernetes.api.model.EnvVar;
import io.fabric8.kubernetes.api.model.VolumeMount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xzy on 2017/2/22.
 */
public class ContainerDto {
    private String name;
    private String image;
    private List<EnvVar> env;
    private List<ContainerPort> ports;
    private List<VolumeMount> volumeMounts;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<EnvVar> getEnv() {
        return env;
    }

    public void setEnv(List<EnvVar> env) {
        this.env = env;
    }

    public List<VolumeMount> getVolumeMounts() {
        return volumeMounts;
    }

    public void setVolumeMounts(List<VolumeMount> volumeMounts) {
        this.volumeMounts = volumeMounts;
    }

    @Override
    public String toString() {
        return "ContainerDto{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", env=" + env +
                '}';
    }

    public static List<ContainerDto> from(List<Container> containers){
        List<ContainerDto> dtos = new ArrayList<>();
        if (containers != null) {
            for (Container container : containers) {
                ContainerDto dto = new ContainerDto();
                dto.setName(container.getName());
                dto.setImage(container.getImage());
                dto.setEnv(container.getEnv());
                dto.setPorts(container.getPorts());
                dto.setVolumeMounts(container.getVolumeMounts());
                dtos.add(dto);
            }
        }
        return dtos;
    }

    public List<ContainerPort> getPorts() {
        return ports;
    }

    public void setPorts(List<ContainerPort> ports) {
        this.ports = ports;
    }
}
