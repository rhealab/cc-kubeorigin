package cc.kubeorigin.kubernetes.deployment;

import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.rs.PodInfo;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.DeploymentStatus;
import io.fabric8.kubernetes.api.model.extensions.RollingUpdateDeployment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
public class DeploymentDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private LabelSelector selector;
    private StatusInfo statusInfo;
    private Map<String, String> labels;
    private Map<String, String> annotations;
    private String strategyType;
    private int minReadySeconds;

    private RollingUpdateDeployment rollingUpdateStrategy;
    private List<String> images;
    private PodInfo podInfo;
    private Date createdOn;

    private List<ContainerDto> containers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public StatusInfo getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(StatusInfo statusInfo) {
        this.statusInfo = statusInfo;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public int getMinReadySeconds() {
        return minReadySeconds;
    }

    public void setMinReadySeconds(int minReadySeconds) {
        this.minReadySeconds = minReadySeconds;
    }

    public RollingUpdateDeployment getRollingUpdateStrategy() {
        return rollingUpdateStrategy;
    }

    public void setRollingUpdateStrategy(RollingUpdateDeployment rollingUpdateStrategy) {
        this.rollingUpdateStrategy = rollingUpdateStrategy;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<ContainerDto> getContainers() {
        return containers;
    }

    public void setContainers(List<ContainerDto> containers) {
        this.containers = containers;
    }

    @Override
    public String toString() {
        return "DeploymentDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", statusInfo=" + statusInfo +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", strategyType='" + strategyType + '\'' +
                ", minReadySeconds=" + minReadySeconds +
                ", rollingUpdateStrategy=" + rollingUpdateStrategy +
                ", images=" + images +
                ", podInfo=" + podInfo +
                ", createdOn=" + createdOn +
                ", containers=" + containers +
                '}';
    }

    public static class StatusInfo {
        private int replicas;
        private int updated;
        private int available;
        private int unavailable;
        private long observedGeneration;

        public int getReplicas() {
            return replicas;
        }

        public void setReplicas(int replicas) {
            this.replicas = replicas;
        }

        public int getUpdated() {
            return updated;
        }

        public void setUpdated(int updated) {
            this.updated = updated;
        }

        public int getAvailable() {
            return available;
        }

        public void setAvailable(int available) {
            this.available = available;
        }

        public int getUnavailable() {
            return unavailable;
        }

        public void setUnavailable(int unavailable) {
            this.unavailable = unavailable;
        }

        public long getObservedGeneration() {
            return observedGeneration;
        }

        public void setObservedGeneration(long observedGeneration) {
            this.observedGeneration = observedGeneration;
        }

        public static StatusInfo from(DeploymentStatus status) {
            StatusInfo statusInfo = new StatusInfo();
            statusInfo.setReplicas(status.getReplicas() == null ? 0 : status.getReplicas());
            statusInfo.setUpdated(status.getUpdatedReplicas() == null ? 0 : status.getUpdatedReplicas());
            statusInfo.setAvailable(status.getAvailableReplicas() == null ? 0 : status.getAvailableReplicas());
            statusInfo.setUnavailable(status.getUnavailableReplicas() == null ? 0 : status.getUnavailableReplicas());
            statusInfo.setObservedGeneration(status.getObservedGeneration() == null ? 0 : status.getObservedGeneration());

            return statusInfo;
        }
    }

    /**
     * @param deployments not null
     * @param allPods     not null
     * @return composed deployment list
     */
    public static List<DeploymentDto> from(List<Deployment> deployments, List<Pod> allPods) {
        List<DeploymentDto> dtos = new ArrayList<>();

        for (Deployment deployment : deployments) {
            //FIXME can not match Expressions
            Map<String, String> selector = deployment.getSpec().getSelector().getMatchLabels();
            List<Pod> pods = CommonUtils.filterPods(allPods, selector);
            dtos.add(from(deployment, pods));
        }

        return dtos;
    }

    /**
     * @param deployment not null
     * @param pods       not null
     * @return composed deployment
     */
    public static DeploymentDto from(Deployment deployment, List<Pod> pods) {
        DeploymentDto dto = new DeploymentDto();
        ObjectMeta metadata = deployment.getMetadata();
        DeploymentSpec spec = deployment.getSpec();
        DeploymentStatus status = deployment.getStatus();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());

        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());
        dto.setAnnotations(metadata.getAnnotations());

        dto.setStatusInfo(StatusInfo.from(status));

        if (spec.getStrategy() != null) {
            dto.setStrategyType(spec.getStrategy().getType());
            dto.setRollingUpdateStrategy(spec.getStrategy().getRollingUpdate());
        }

        Integer minReadySeconds = spec.getMinReadySeconds();
        dto.setMinReadySeconds(minReadySeconds != null ? minReadySeconds : 0);

        List<String> images = new ArrayList<>();
        List<Container> containers = spec.getTemplate().getSpec().getContainers();
        for (Container container : containers) {
            images.add(container.getImage());
        }
        dto.setImages(images);

        dto.setContainers(ContainerDto.from(containers));

        PodInfo podInfo = CommonUtils.composePodInfo(spec.getReplicas(), status.getReplicas(), pods);
        dto.setPodInfo(podInfo);

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        long age = System.currentTimeMillis() - dto.getCreatedOn().getTime();
        dto.setAge(age);

        return dto;
    }
}
