package cc.kubeorigin.kubernetes.statefulset;

import cc.kubeorigin.KubernetesProperties;
import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static cc.kubeorigin.common.utils.OkHttpClientUtils.JSON;
import static cc.kubeorigin.common.utils.OkHttpClientUtils.getUnsafeOkHttpClient;

/**
 * @author Created by mrzihan.tang@gmail.com on 05/07/2017.
 */
@Component
public class StatefulSetManagement {
    private static final Logger logger = LoggerFactory.getLogger(StatefulSetManagement.class);
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    @Inject
    private KubernetesProperties properties;

    private String token;

    @PostConstruct
    private void init() {
        if (!properties.isLoginWithToken()) {
            String str = properties.getAdmin().getUsername() + ":" + properties.getAdmin().getPassword();
            token = "Basic " + Base64.getEncoder().encodeToString(str.getBytes());
        } else {
            token = "Bearer " + properties.getToken();
        }
    }

    public List<StatefulSet> getStatefulSetList(String nsName) {
        KubernetesClient client = clientManager.getClient();
        List<StatefulSet> ret = client.apps().statefulSets().inNamespace(nsName).list().getItems();
        return ret == null ? new ArrayList<>() : ret;
    }

    public List<StatefulSet> getStatefulSetsOfApp(String namespace, List<String> appNames) {
        return KubernetesUtils.getStatefulSetsOfApps(clientManager.getClient(), namespace, appNames);
    }

    public StatefulSet getStatefulSet(String nsName, String statefulSetName) {
        KubernetesClient client = clientManager.getClient();
        return client.apps().statefulSets().inNamespace(nsName).withName(statefulSetName).get();
    }

    public List<Pod> getStatefulSetPodList(String nsName, StatefulSet statefulSet) {
        List<Pod> ret = new ArrayList<>();
        LabelSelector selector = statefulSet.getSpec().getSelector();
        if (selector != null) {
            ret = podsManagement.getPodsByLabels(nsName, selector.getMatchLabels());
        }
        return ret;
    }

    public StatefulSet update(String namespaceName, StatefulSet statefulSet) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String statefulSetName = statefulSet.getMetadata().getName();
        StatefulSet oldStatefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName, statefulSetName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldStatefulSet, ResourceType.STATEFULSET);

        resourceHelper.removeAppLabel(statefulSet);

//        KubernetesClient client = clientManager.getClient();
//        statefulSet = client.apps().statefulSets().inNamespace(namespaceName).withName(statefulSetName).replace(statefulSet);
        statefulSet = updateK8sStatefulSet(namespaceName, statefulSetName, statefulSet);


        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.REPLICATION_CONTROLLER)
                .resourceName(statefulSetName)
                .extras(ImmutableMap.of("originName", statefulSetName,
                        "currentName", statefulSet.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return statefulSet;
    }

    public boolean deleteStatefulSet(String namespaceName, String statefulSetName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        StatefulSet statefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName, statefulSetName);

        resourceHelper.validateResourceIsOrigin(namespaceName, statefulSet, ResourceType.STATEFULSET);

        int replicas = statefulSet.getSpec().getReplicas();

        statefulSet.getSpec().setReplicas(0);
        statefulSet = updateK8sStatefulSet(namespaceName, statefulSetName, statefulSet);

        boolean deleted = false;
        if (statefulSet.getSpec().getReplicas().equals(0)) {
            KubernetesClient client = clientManager.getClient();
            for (int i = 0; i < replicas; i++) {
                String podName = statefulSetName + "-" + String.valueOf(i);
                client.pods().inNamespace(namespaceName).withName(podName).delete();
            }
            deleted = deleteK8sStatefulSet(namespaceName, statefulSetName);
        }

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.STATEFULSET)
                .resourceName(statefulSetName)
                .extra("deleted", deleted)
                .build();
        operationAuditMessageProducer.send(message);

        return deleted;
    }

    public boolean deleteK8sStatefulSet(String namespaceName, String statefulSetName) {
        String subUrl = "/apis/apps/v1beta1/namespaces/" + namespaceName
                + "/statefulsets/" + statefulSetName;
        Request request = new Request.Builder()
                .header("Authorization", token)
                .url(properties.getMasterUrl() + subUrl)
                .delete()
                .build();

        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (!response.isSuccessful()) {
                logger.error("delete statefulset failed - {}",
                        responseBody == null ? response.message() : responseBody.toString());
            }

            if (responseBody != null) {
                response.close();
            }

            return response.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg", e.getCause()));
        }
    }

    public StatefulSet updateK8sStatefulSet(String namespaceName, String statefulSetName, StatefulSet statefulSet) {
        String subUrl = "/apis/apps/v1beta1/namespaces/" + namespaceName + "/statefulsets/" + statefulSetName;
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
        String json;
        try {
            json = mapper.writeValueAsString(statefulSet);
        } catch (JsonProcessingException e) {
            throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg", e.getCause()));
        }

        logger.info(json);
        RequestBody body = RequestBody.create(JSON, json);


        Response response = execute(subUrl, HttpMethod.PUT, body);
        if (response.isSuccessful()) {
            ObjectMapper readMapper = new ObjectMapper();
            readMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                statefulSet = readMapper.readValue(response.body().string(), StatefulSet.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            statefulSet = getStatefulSet(namespaceName, statefulSetName);
        }
        return statefulSet;
    }

    public enum HttpMethod {
        POST, GET, DELETE, PUT, PATCH
    }

    public Response execute(String subUrl,
                            HttpMethod requestMethod,
                            RequestBody requestBody) {
        Request request = new Request.Builder()
                .header("Authorization", token)
                .url(properties.getMasterUrl() + subUrl)
                .method(requestMethod.name(), requestBody)
                .build();
        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("response code:" + response.code() + ",response msg:" + response.message());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg", e.getCause()));
        }
    }
}
