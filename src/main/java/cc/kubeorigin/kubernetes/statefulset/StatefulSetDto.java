package cc.kubeorigin.kubernetes.statefulset;


import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.deployment.ContainerDto;
import cc.kubeorigin.kubernetes.rs.PodInfo;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.RollingUpdateDeployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Created by mrzihan.tang@gmail.com on 05/07/2017.
 */
public class StatefulSetDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private LabelSelector selector;
    private Map<String, String> annotations;
    private StatusInfo statusInfo;
    private Map<String, String> labels;
    private String strategyType;

    private RollingUpdateDeployment rollingUpdateStrategy;
    private List<String> images;
    private PodInfo podInfo;
    private Date createdOn;

    private List<ContainerDto> containers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public StatusInfo getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(StatusInfo statusInfo) {
        this.statusInfo = statusInfo;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }


    public RollingUpdateDeployment getRollingUpdateStrategy() {
        return rollingUpdateStrategy;
    }

    public void setRollingUpdateStrategy(RollingUpdateDeployment rollingUpdateStrategy) {
        this.rollingUpdateStrategy = rollingUpdateStrategy;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<ContainerDto> getContainers() {
        return containers;
    }

    public void setContainers(List<ContainerDto> containers) {
        this.containers = containers;
    }

    @Override
    public String toString() {
        return "StatefulSetDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", annotations=" + annotations +
                ", statusInfo=" + statusInfo +
                ", labels=" + labels +
                ", strategyType='" + strategyType + '\'' +
                ", rollingUpdateStrategy=" + rollingUpdateStrategy +
                ", images=" + images +
                ", podInfo=" + podInfo +
                ", createdOn=" + createdOn +
                ", containers=" + containers +
                '}';
    }

    public static class StatusInfo {
        private int replicas;
        private long observedGeneration;

        public int getReplicas() {
            return replicas;
        }

        public void setReplicas(int replicas) {
            this.replicas = replicas;
        }

        public long getObservedGeneration() {
            return observedGeneration;
        }

        public void setObservedGeneration(long observedGeneration) {
            this.observedGeneration = observedGeneration;
        }

        @Override
        public String toString() {
            return "StatusInfo{" +
                    "replicas=" + replicas +
                    ", observedGeneration=" + observedGeneration +
                    '}';
        }

        public static StatusInfo from(StatefulSetStatus status) {
            StatusInfo ret = new StatusInfo();
            ret.setReplicas(status.getReplicas() == null ? 0 : status.getReplicas());
            ret.setObservedGeneration(status.getObservedGeneration() == null ? 0 : status.getObservedGeneration());
            return ret;
        }
    }

    public static List<StatefulSetDto> from(List<StatefulSet> statefulSetList, List<Pod> podList) {
        List<StatefulSetDto> ret = new ArrayList<>();

        for (StatefulSet statefulSet : statefulSetList) {
            Map<String, String> selector = statefulSet.getSpec().getSelector().getMatchLabels();
            List<Pod> matchedPodList = CommonUtils.filterPods(podList, selector);
            ret.add(from(statefulSet, matchedPodList));
        }

        return ret;
    }

    public static StatefulSetDto from(StatefulSet statefulSet, List<Pod> podList) {
        StatefulSetDto ret = new StatefulSetDto();
        ObjectMeta metaData = statefulSet.getMetadata();
        StatefulSetSpec spec = statefulSet.getSpec();
        StatefulSetStatus status = statefulSet.getStatus();

        // Extract meta data
        ret.setName(metaData.getName());
        ret.setUid(metaData.getUid());
        ret.setNamespace(metaData.getNamespace());
        ret.setLabels(metaData.getLabels());
        ret.setAnnotations(metaData.getAnnotations());
        ret.setCreatedOn(DateUtils.parseK8sDate(metaData.getCreationTimestamp()));
        // Extract spec
        ret.setSelector(spec.getSelector());
        List<String> urlImageList = new ArrayList<>();
        List<Container> containerList = spec.getTemplate().getSpec().getContainers();
        for (Container container : containerList) {
            urlImageList.add(container.getImage());
        }

        ret.setContainers(ContainerDto.from(containerList));

        ret.setImages(urlImageList);
        PodInfo podInfo = CommonUtils.composePodInfo(spec.getReplicas(), status.getReplicas(), podList);
        ret.setPodInfo(podInfo);
        // Extract status info
        ret.setStatusInfo(StatusInfo.from(status));
        // Other
        ret.setAge(DateUtils.getAge(ret.getCreatedOn()));
        return ret;
    }
}
