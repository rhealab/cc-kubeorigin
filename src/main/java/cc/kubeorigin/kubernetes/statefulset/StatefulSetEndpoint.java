package cc.kubeorigin.kubernetes.statefulset;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Created by mrzihan.tang@gmail.com on 05/07/2017.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{nsName}/statefulsets")
@Api(value = "StatefulSet", description = "Operation about statefulset.", produces = "application/json")
public class StatefulSetEndpoint {
    @Inject
    private StatefulSetManagement statefulSetManagement;
    @Inject
    private PodsManagement podsManagement;

    @GET
    @ApiOperation(value = "List statefulsets in namespace.", responseContainer = "List", response = StatefulSetDto.class)
    public Response getStatefulSetList(@PathParam("nsName") String nsName) {
        List<StatefulSet> statefulsetList = statefulSetManagement.getStatefulSetList(nsName);
        if (!statefulsetList.isEmpty()) {
            List<Pod> podList = podsManagement.getPods(nsName);
            return Response.ok(StatefulSetDto.from(statefulsetList, podList)).build();
        }
        return Response.ok(new ArrayList<StatefulSetDto>()).build();
    }

    @POST
    @ApiOperation(value = "List statefulsets in namespace belong to given apps.", responseContainer = "List", response = StatefulSetDto.class)
    public Response getStatefulSetsOfApps(@PathParam("nsName") String nsName, List<String> appNames) {
        List<StatefulSet> statefulsetList = statefulSetManagement.getStatefulSetsOfApp(nsName, appNames);
        if (!statefulsetList.isEmpty()) {
            List<Pod> podList = podsManagement.getPods(nsName);
            return Response.ok(StatefulSetDto.from(statefulsetList, podList)).build();
        }
        return Response.ok(new ArrayList<StatefulSetDto>()).build();
    }

    @GET
    @Path("origin_list")
    @ApiOperation(value = "List statefulsets in namespace, with k8s original form", responseContainer = "List", response = StatefulSet.class)
    public Response getStatefulSetListOrigin(@PathParam("nsName") String nsName) {
        List<StatefulSet> statefulsets = statefulSetManagement.getStatefulSetList(nsName);
        return Response.ok(statefulsets).build();
    }

    @GET
    @Path("{statefulsetName}")
    @ApiOperation(value = "Get statefulset info.", response = StatefulSetDto.class)
    public Response getStatefulSet(
            @PathParam("nsName") String nsName,
            @PathParam("statefulsetName") String statefulsetName) {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(nsName, statefulsetName);
        if (statefulSet != null) {
            List<Pod> podList = statefulSetManagement.getStatefulSetPodList(nsName, statefulSet);
            return Response.ok(StatefulSetDto.from(statefulSet, podList)).build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.STATEFULSET_NOT_FOUND)
                    .payload(ImmutableMap.of("namespace", nsName,
                            "statefulset", statefulsetName))
                    .build();
        }
    }

    @GET
    @Path("{statefulsetName}/origin")
    @ApiOperation(value = "Get statefulset info of k8s original format.", response = StatefulSet.class)
    public Response getStatefulSetOrigin(
            @PathParam("nsName") String nsName,
            @PathParam("statefulsetName") String statefulsetName) {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(nsName, statefulsetName);
        if (statefulSet != null) {
            return Response.ok(statefulSet).build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.STATEFULSET_NOT_FOUND)
                    .payload(ImmutableMap.of("namespace", nsName,
                            "statefulset", statefulsetName))
                    .build();
        }
    }

    @PUT
    @ApiOperation(value = "Update statefulset by json.", response = StatefulSetDto.class)
    public Response update(@PathParam("nsName") String namespaceName,
                           StatefulSet statefulSet) {
        StatefulSet result = statefulSetManagement.update(namespaceName, statefulSet);
        List<Pod> pods = podsManagement.getPodsByStatefulSet(namespaceName, result.getMetadata().getName());
        return Response.ok(StatefulSetDto.from(result, pods)).build();
    }

    @DELETE
    @Path("{statefulSetName}")
    @ApiOperation(value = "Delete statefulset.")
    public Response deleteStatefulSet(@PathParam("nsName") String namespaceName,
                                      @PathParam("statefulSetName") String statefulsetName) {
        boolean deleted = statefulSetManagement.deleteStatefulSet(namespaceName, statefulsetName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "statefulset", statefulsetName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.STATEFULSET_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "statefulset", statefulsetName))
                    .build();
        }
    }
}
