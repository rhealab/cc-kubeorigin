package cc.kubeorigin.kubernetes.log;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/pods")
@Api(value = "Log", description = "Operation about logs.", produces = "application/json")
public class LogsEndpoint {

    @Inject
    private LogsManagement logsManagement;


    @GET
    @Path("{podName}/container/{containerName}/logs")
    @ApiOperation(value = "Get container's log.", response = PodLogDto.class)
    public Response getPodLogs(@PathParam("namespaceName") String namespaceName,
                               @PathParam("podName") String podName,
                               @PathParam("containerName") String containerName,
                               @QueryParam("limitBytes") Integer limit) {

        PodLogDto logDto;
        if (limit != null) {
            logDto = logsManagement.getPodLogs(namespaceName, podName, containerName, limit);
        } else {
            logDto = logsManagement.getPodLogs(namespaceName, podName, containerName);
        }

        if (logDto != null) {
            return Response.ok(logDto).build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("{podName}/container/{containerName}/logs/csv")
    @ApiOperation(value = "Get container's log.", response = PodLogDto.class)
    public Response getPodLogsCsv(@PathParam("namespaceName") String namespaceName,
                               @PathParam("podName") String podName,
                               @PathParam("containerName") String containerName) {

        PodLogDto logDto = logsManagement.getPodLogs(namespaceName, podName, containerName, 1024 * 1024);
        File file = logsManagement.toCSVFile(logDto.getLogs());
        return Response.ok(file).build();
    }

    @GET
    @Path("{podName}/container/{containerName}/logs/origin")
    @ApiOperation(value = "Get container's log.", response = String.class)
    public Response getPodLogs(@PathParam("namespaceName") String namespaceName,
                               @PathParam("podName") String podName,
                               @PathParam("containerName") String containerName) {
        return Response.ok(logsManagement.getPodLogOrigin(namespaceName, podName, containerName)).build();
    }

}
