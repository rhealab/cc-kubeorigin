package cc.kubeorigin.kubernetes.log;

import java.util.List;

/**
 * Created by zzb on 2017/2/22.
 */

public class PodLogDto {
    private List<String> logs;
    private boolean isExceedMaxSize = false;

    public List<String> getLogs() {
        return logs;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    public static PodLogDto from(List<String> logs) {
        PodLogDto podLogDto = new PodLogDto();
        podLogDto.setLogs(logs);
        return podLogDto;
    }

    public boolean isExceedMaxSize() {
        return isExceedMaxSize;
    }

    public void setExceedMaxSize(boolean exceedMaxSize) {
        isExceedMaxSize = exceedMaxSize;
    }
}
