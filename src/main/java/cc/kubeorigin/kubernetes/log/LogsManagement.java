package cc.kubeorigin.kubernetes.log;

import cc.kubeorigin.KubernetesProperties;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import static cc.kubeorigin.common.utils.OkHttpClientUtils.getUnsafeOkHttpClient;
import static com.google.common.collect.ImmutableSortedMap.of;

@Component
public class LogsManagement {

    private static final Logger logger = LoggerFactory.getLogger(LogsManagement.class);
    private static final int MAX_LOGS_BYTES_SIZE = 14496160;

    @Inject
    private KubernetesProperties properties;

    @Inject
    private KubernetesClientManager clientManager;

    private String token;

    @PostConstruct
    private void init() {
        if (!properties.isLoginWithToken()) {
            String str = properties.getAdmin().getUsername() + ":" + properties.getAdmin().getPassword();
            token = "Basic " + Base64.getEncoder().encodeToString(str.getBytes());
        } else {
            token = "Bearer " + properties.getToken();
        }
    }

    public enum HttpMethod {
        POST, GET, DELETE, PUT, PATCH
    }


    public PodLogDto getPodLogs(String namespaceName, String podName, String containerName) {
        String subUrl = "/api/v1/namespaces/" + namespaceName + "/pods/" + podName + "/log";

        HttpUrl url = HttpUrl
                .parse(properties.getMasterUrl() + subUrl).newBuilder()
                .addQueryParameter("container", containerName)
                .addQueryParameter("timestamps", "true")
                .addQueryParameter("limitBytes", String.valueOf(MAX_LOGS_BYTES_SIZE)) // max size 10M
                .build();

        return performLogRequest(url);
    }

    public PodLogDto getPodLogs(String namespaceName, String podName, String containerName, Integer length) {
        String subUrl = "/api/v1/namespaces/" + namespaceName + "/pods/" + podName + "/log";

        HttpUrl url = HttpUrl
                .parse(properties.getMasterUrl() + subUrl).newBuilder()
                .addQueryParameter("container", containerName)
                .addQueryParameter("timestamps", "true")
                .addQueryParameter("limitBytes", length.toString())
                .build();

        return performLogRequest(url);
    }

    private PodLogDto performLogRequest(HttpUrl url) {
        Request request = new Request.Builder()
                .header("Authorization", token)
                .url(url)
                .method("GET", null)
                .build();
        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("requestId: " + UserContext.getRequestId() + "request url:" + url + "response code:" + response.code() + ",response msg:" + response.message());
            String res = response.body().string();
            int len = res.getBytes("UTF-8").length;
            logger.info("response bytes" + len);

            if(!response.isSuccessful()) {
                logger.info("Error Occur: \n requestId: " + UserContext.getRequestId() + "\nException:\n", res);
                throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg",  res));
            }

            List<String> logs = splitLogs(res);
            PodLogDto dto = PodLogDto.from(logs);
            if (len > MAX_LOGS_BYTES_SIZE - 100) {
                dto.setExceedMaxSize(true);
            }
            if (response.body() != null) {
                response.close();
            }
            return dto;
        } catch (IOException e) {
            logger.info("Error Occur: \n requestId: " + UserContext.getRequestId() + "\nException:\n", e);
            throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg", e.getCause()));
        }
    }

    private List<String> splitLogs(String string) {
        String[] arr = string.split("\n");
        return Arrays.asList(arr);
    }

    public String getPodLogOrigin(String namespaceName, String podName, String containerName) {
        String ret;
        try {
            ret = clientManager.getClient().pods().inNamespace(namespaceName).withName(podName).inContainer(containerName).getLog();
        } catch (Exception e) {
            e.printStackTrace();
            throw new CcException(KubeoriginReturnCodeNameConstants.K8S_SERVER_ERROR, ImmutableMap.of("msg", e.getCause()));
        }
        return ret;
    }

    public File toCSVFile(List<String> logHits) {
        // create file in tmp dir
        String path = "/tmp/" + RandomStringUtils.random(8, true, true) + ".csv";
        File f = new File(path);
        f.getParentFile().mkdirs();
        logger.info("csv saved to " + path);

        // write file with csv data
        try {
            FileWriter out = new FileWriter(path);
            CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT);
            f.createNewFile();
            try {
                for (String log: logHits) {
                    printer.printRecord(log);
                }
            } finally {
                out.close();
                printer.close();
            }
        } catch (IOException e) {
            throw new CcException(KubeoriginReturnCodeNameConstants.export_csv___failed, of("cause", e.toString()));
        }
        return f;
    }

}
