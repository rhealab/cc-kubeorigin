package cc.kubeorigin.kubernetes;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public enum Kind {
    Deployment,
    Endpoint,
    Event,
    Pod,
    ReplicationController,
    ReplicaSet,
    Service
}
