package cc.kubeorigin.kubernetes.event;

import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Event;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/events")
@Api(value = "Event", description = "Operation about events.", produces = "application/json")
public class EventEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(EventEndpoint.class);

    @Inject
    private EventManagement eventManagement;

    @GET
    @ApiOperation(value = "List events in namespace.", responseContainer = "List", response = EventDto.class)
    public Response getEvents(@PathParam("namespaceName") String namespaceName) {
        List<Event> events = eventManagement.getEvents(namespaceName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("origin")
    @ApiOperation(value = "List events in namespace of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsOrigin(@PathParam("namespaceName") String namespaceName) {
        List<Event> events = eventManagement.getEvents(namespaceName);
        return Response.ok(events).build();
    }

    @GET
    @Path("{eventName}")
    @ApiOperation(value = "Get event info.", response = EventDto.class)
    public Response getEvents(@PathParam("namespaceName") String namespaceName,
                              @PathParam("eventName") String eventName) {
        Event event = eventManagement.getEventByName(namespaceName, eventName);
        if (event != null) {
            return Response.ok(EventDto.from(event)).build();
        }
        return new ReturnCodeResponseBuilder()
                .codeName(KubeoriginReturnCodeNameConstants.EVENT_NOT_FOUND)
                .payload(ImmutableMap.of("namespace", namespaceName,
                        "event", eventName)).
                        build();
    }

    @GET
    @Path("{eventName}/origin")
    @ApiOperation(value = "Get event info of k8s original format.", response = Event.class)
    public Response getEventsOrigin(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("eventName") String eventName) {
        Event event = eventManagement.getEventByName(namespaceName, eventName);
        if (event != null) {
            return Response.ok(event).build();
        }
        return new ReturnCodeResponseBuilder()
                .codeName(KubeoriginReturnCodeNameConstants.EVENT_NOT_FOUND)
                .payload(ImmutableMap.of("namespace", namespaceName,
                        "event", eventName)).
                        build();
    }

    @DELETE
    @Path("{eventName}")
    @ApiOperation(value = "Delete event.")
    public Response deleteEvents(@PathParam("namespaceName") String namespaceName,
                                 @PathParam("eventName") String eventName) {
        boolean deleted = eventManagement.deleteEvents(namespaceName, eventName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "event", eventName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.EVENT_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "event", eventName))
                    .build();
        }
    }

    @GET
    @Path("by_rc/{rcName}")
    @ApiOperation(value = "Get events by rc.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByRc(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("rcName") String rcName) {
        List<Event> events = eventManagement.getEventsByRc(namespaceName, rcName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_rc/{rcName}/origin")
    @ApiOperation(value = "Get events by rc of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByRcOrigin(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("rcName") String rcName) {
        List<Event> events = eventManagement.getEventsByRc(namespaceName, rcName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_rs/{rsName}")
    @ApiOperation(value = "Get events by rs.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByRs(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("rsName") String rsName) {
        List<Event> events = eventManagement.getEventsByRs(namespaceName, rsName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_rs/{rsName}/origin")
    @ApiOperation(value = "Get events by rs of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByRsOrigin(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("rsName") String rsName) {
        List<Event> events = eventManagement.getEventsByRs(namespaceName, rsName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_service/{svcName}")
    @ApiOperation(value = "Get events by service.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByService(@PathParam("namespaceName") String namespaceName,
                                       @PathParam("svcName") String svcName) {
        List<Event> events = eventManagement.getEventsByService(namespaceName, svcName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_service/{svcName}/origin")
    @ApiOperation(value = "Get events by service of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByServiceOrigin(@PathParam("namespaceName") String namespaceName,
                                             @PathParam("svcName") String svcName) {
        List<Event> events = eventManagement.getEventsByService(namespaceName, svcName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get events by deployment.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByDeployment(@PathParam("namespaceName") String namespaceName,
                                          @PathParam("deploymentName") String deploymentName) {
        List<Event> events = eventManagement.getEventsByDeployment(namespaceName, deploymentName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_deployment/{deploymentName}/origin")
    @ApiOperation(value = "Get events by deployment of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByDeploymentOrigin(@PathParam("namespaceName") String namespaceName,
                                                @PathParam("deploymentName") String deploymentName) {
        List<Event> events = eventManagement.getEventsByDeployment(namespaceName, deploymentName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_statefulset/{statefulSetName}")
    @ApiOperation(value = "Get events by statefulset.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("statefulSetName") String statefulSetName) {
        List<Event> events = eventManagement.getEventsByStatefulSet(namespaceName, statefulSetName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_statefulset/{statefulSetName}/origin")
    @ApiOperation(value = "Get events by statefulset of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByStatefulSetOrigin(@PathParam("namespaceName") String namespaceName,
                                                 @PathParam("statefulSetName") String statefulSetName) {
        List<Event> events = eventManagement.getEventsByStatefulSet(namespaceName, statefulSetName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_pod/{podName}")
    @ApiOperation(value = "Get events by pod.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByPod(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("podName") String podName) {
        List<Event> events = eventManagement.getEventsByPod(namespaceName, podName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_pod/{podName}/origin")
    @ApiOperation(value = "Get events by pod of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByPodOrigin(@PathParam("namespaceName") String namespaceName,
                                         @PathParam("podName") String podName) {
        List<Event> events = eventManagement.getEventsByPod(namespaceName, podName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_endpoint/{epName}")
    @ApiOperation(value = "Get events by endpoint.", responseContainer = "List", response = EventDto.class)
    public Response getEventsByEp(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("epName") String epName) {
        List<Event> events = eventManagement.getEventsByEndpoint(namespaceName, epName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_endpoint/{epName}/origin")
    @ApiOperation(value = "Get events by endpoint of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getEventsByEpOrigin(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("epName") String epName) {
        List<Event> events = eventManagement.getEventsByEndpoint(namespaceName, epName);
        return Response.ok(events).build();
    }

    //    @PUT
    public Response update(@PathParam("namespaceName") String namespaceName,
                           Event event) {
        Event result = eventManagement.update(namespaceName, event);
        return Response.ok(EventDto.from(result)).build();
    }
}
