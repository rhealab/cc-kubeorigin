package cc.kubeorigin.kubernetes.event;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Component
public class EventManagement {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<Event> getEvents(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<Event> events = client.events().inNamespace(namespaceName).list().getItems();
        return events == null ? new ArrayList<>() : events;
    }

    public Event getEventByName(String namespaceName, String eventName) {
        KubernetesClient client = clientManager.getClient();
        return client.events().inNamespace(namespaceName).withName(eventName).get();
    }

    public boolean deleteEvents(String namespaceName, String eventName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.events().inNamespace(namespaceName).withName(eventName).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.EVENT)
                .resourceName(eventName)
                .extra("deleted", deleted)
                .build();
        //TODO need to talk with junting
//        operationAuditMessageProducer.send(message);

        return deleted;
    }

    public List<Event> getEventsByRc(String namespaceName, String rcName) {
        ReplicationController rc = resourceHelper.validateRcShouldExists(namespaceName, rcName);
        return getEvents(namespaceName, rc);
    }

    public List<Event> getEventsByRs(String namespaceName, String rsName) {
        ReplicaSet replicaSet = resourceHelper.validateRsShouldExists(namespaceName, rsName);
        return getEvents(namespaceName, replicaSet);
    }

    public List<Event> getEventsByEndpoint(String namespaceName, String epName) {
        Endpoints endpoints = resourceHelper.validateEndpointsShouldExists(namespaceName, epName);
        return getEvents(namespaceName, endpoints);
    }

    public List<Event> getEventsByDeployment(String namespaceName, String deploymentName) {
        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName, deploymentName);
        return getEvents(namespaceName, deployment);
    }

    public List<Event> getEventsByService(String namespaceName, String serviceName) {
        Service service = resourceHelper.validateServiceShouldExists(namespaceName, serviceName);
        return getEvents(namespaceName, service);
    }

    public List<Event> getEventsByStatefulSet(String namespaceName, String statefulSetName) {
        StatefulSet statefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName, statefulSetName);
        return getEvents(namespaceName, statefulSet);
    }

    public List<Event> getEventsByPod(String namespaceName, String podName) {
        Pod pod = resourceHelper.validatePodShouldExists(namespaceName, podName);
        return getEvents(namespaceName, pod);
    }

    private List<Event> getEvents(String namespaceName, HasMetadata resource) {
        String uid = resource.getMetadata().getUid();
        List<Event> eventList = null;
        if (!Strings.isNullOrEmpty(uid)) {
            eventList = clientManager.getClient().events()
                    .inNamespace(namespaceName).withField("involvedObject.uid", uid).list().getItems();
        }
        return eventList == null ? new ArrayList<>() : eventList;
    }

    public Event update(String namespaceName, Event event) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String eventName = event.getMetadata().getName();
        Event oldEvent = resourceHelper.validateEventShouldExists(namespaceName, eventName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldEvent, ResourceType.EVENT);

        resourceHelper.removeAppLabel(event);

        KubernetesClient client = clientManager.getClient();
        event = client.events().inNamespace(namespaceName).withName(eventName).replace(event);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.EVENT)
                .resourceName(eventName)
                .extras(ImmutableMap.of("originName", eventName,
                        "currentName", event.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return event;
    }
}
