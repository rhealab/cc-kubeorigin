package cc.kubeorigin.kubernetes.service;

import cc.kubeorigin.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xzy on 2017/2/22.
 */
public class ServiceDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private Map<String, String> selector;
    private Map<String, String> labels;
    private Map<String, String> annotations;
    private String type;

    private String clusterIP;
    private List<String> externalIPs;

    private List<SvcEndpointDto> internalEndpoint;
    private List<SvcEndpointDto> externalEndpoints;

    private List<ServicePort> ports;
    private List<LoadBalancerIngress> ingress;
    private Date createdOn;
    private List<String> links;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<SvcEndpointDto> getInternalEndpoint() {
        return internalEndpoint;
    }

    public void setInternalEndpoint(List<SvcEndpointDto> internalEndpoint) {
        this.internalEndpoint = internalEndpoint;
    }

    public List<SvcEndpointDto> getExternalEndpoints() {
        return externalEndpoints;
    }

    public void setExternalEndpoints(List<SvcEndpointDto> externalEndpoints) {
        this.externalEndpoints = externalEndpoints;
    }

    public List<ServicePort> getPorts() {
        return ports;
    }

    public void setPorts(List<ServicePort> ports) {
        this.ports = ports;
    }

    public List<LoadBalancerIngress> getIngress() {
        return ingress;
    }

    public void setIngress(List<LoadBalancerIngress> ingress) {
        this.ingress = ingress;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "ServiceDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", type='" + type + '\'' +
                ", clusterIP='" + clusterIP + '\'' +
                ", externalIPs=" + externalIPs +
                ", internalEndpoint=" + internalEndpoint +
                ", externalEndpoints=" + externalEndpoints +
                ", ports=" + ports +
                ", ingress=" + ingress +
                ", createdOn=" + createdOn +
                ", links=" + links +
                '}';
    }

    public static class SvcEndpointDto {
        private String host;
        private List<ServicePort> ports;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public List<ServicePort> getPorts() {
            return ports;
        }

        public void setPorts(List<ServicePort> ports) {
            this.ports = ports;
        }

        @Override
        public String toString() {
            return "SvcEndpointDto{" +
                    "host='" + host + '\'' +
                    ", ports=" + ports +
                    '}';
        }
    }

    /**
     * @param services not null
     * @return composed service list
     */
    public static List<ServiceDto> from(List<Service> services, String vipHost) {
        List<ServiceDto> dtos = new ArrayList<>();
        for (Service service : services) {
            dtos.add(from(service, vipHost));
        }
        return dtos;
    }

    /**
     * @param service not null
     * @return composed service
     */
    public static ServiceDto from(Service service, String vipHost) {
        ObjectMeta metadata = service.getMetadata();
        ServiceSpec spec = service.getSpec();
        ServiceStatus status = service.getStatus();
        ServiceDto dto = new ServiceDto();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());
        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());
        dto.setAnnotations(metadata.getAnnotations());
        dto.setType(spec.getType());

        dto.setClusterIP(spec.getClusterIP());
        List<String> externalIPs = spec.getExternalIPs();
        dto.setExternalIPs(externalIPs);

        dto.setPorts(spec.getPorts());
        if (status != null && status.getLoadBalancer() != null) {
            dto.setIngress(status.getLoadBalancer().getIngress());
        }

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        long age = DateUtils.getAge(dto.getCreatedOn());
        dto.setAge(age);

        composeSvcEndpointDto(dto, externalIPs);
        dto.setLinks(ServiceHelper.composeLink(service, vipHost));

        return dto;
    }

    private static void composeSvcEndpointDto(ServiceDto dto, List<String> externalIPs) {
        List<SvcEndpointDto> internalList = new ArrayList<>();
        SvcEndpointDto internal = new SvcEndpointDto();
        internal.setHost(dto.getName() + "." + dto.getNamespace());
        internal.setPorts(dto.getPorts());
        internalList.add(internal);
        dto.setInternalEndpoint(internalList);

        if (!CollectionUtils.isEmpty(externalIPs)) {
            List<SvcEndpointDto> externalList = new ArrayList<>();
            for (String ip : externalIPs) {
                SvcEndpointDto external = new SvcEndpointDto();
                external.setHost(ip);
                external.setPorts(dto.getPorts());
                externalList.add(external);
            }
            dto.setExternalEndpoints(externalList);
        }
    }
}