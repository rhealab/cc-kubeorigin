package cc.kubeorigin.kubernetes.service;

import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServicePort;
import io.fabric8.kubernetes.api.model.ServiceSpec;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/14.
 */
public class ServiceHelper {

    public static List<String> composeLink(Service service, String vipHost) {
        Set<String> links = new HashSet<>();
        String serviceType = service.getSpec().getType();
        List<ServicePort> ports = Optional
                .ofNullable(service.getSpec().getPorts())
                .orElseGet(ArrayList::new);

        if (ServiceType.NodePort.name().equals(serviceType)) {
            Set<String> nodePortLinks = ports
                    .stream()
                    .map(ServicePort::getNodePort)
                    .filter(Objects::nonNull)
                    .map(p -> vipHost + ":" + p)
                    .collect(Collectors.toSet());
            links.addAll(nodePortLinks);
        }

        List<String> externalIPs = service.getSpec().getExternalIPs();
        if (!CollectionUtils.isEmpty(externalIPs)) {
            Set<String> externalIpLinks = externalIPs
                    .stream()
                    .map(ip -> ports.stream()
                            .map(ServicePort::getPort)
                            .filter(Objects::nonNull)
                            .map(p -> ip + ":" + p)
                            .collect(Collectors.toList()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());
            links.addAll(externalIpLinks);
        }

        return new ArrayList<>(links);
    }

    public static ServiceType getFormType(Service service) {
        return Optional.ofNullable(service)
                .map(Service::getSpec)
                .map(ServiceSpec::getType)
                .map(ServiceType::valueOf)
                .orElse(null);
    }
}
