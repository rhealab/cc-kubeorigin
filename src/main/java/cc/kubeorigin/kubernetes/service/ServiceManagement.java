package cc.kubeorigin.kubernetes.service;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.CommonUtils;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by xzy on 2017/3/3.
 */
@Component
public class ServiceManagement {
    private static final Logger logger = LoggerFactory.getLogger(ServiceManagement.class);
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ResourceHelper resourceHelper;

    public List<Service> getServiceList(String namespaceName) {
        KubernetesClient client = clientManager.getClient();
        List<Service> services = client.services().inNamespace(namespaceName).list().getItems();
        return services == null ? new ArrayList<>() : services;
    }

    public Service getService(String namespaceName, String name) {
        KubernetesClient client = clientManager.getClient();
        return client.services().inNamespace(namespaceName).withName(name).get();
    }

    public List<Service> getServicesOfApps(String namespaceName, List<String> appNames) {
        return KubernetesUtils.getServicesOfApps(clientManager.getClient(), namespaceName, appNames);
    }

    public boolean deleteService(String namespaceName, String name) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Service service=resourceHelper.validateServiceShouldExists(namespaceName,name);

        resourceHelper.validateResourceIsOrigin(namespaceName,service,ResourceType.SERVICE);

        KubernetesClient client = clientManager.getClient();
        boolean deleted = client.services().inNamespace(namespaceName).withName(name).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.SERVICE)
                .resourceName(name)
                .extra("deleted", deleted)
                .build();
        operationAuditMessageProducer.send(message);

        return deleted;
    }

    public List<Service> getServicesByDeployment(String namespaceName, String deploymentName) {
        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName,deploymentName);

        List<Service> services = new ArrayList<>();
        Map<String, String> labels = deployment.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            services = CommonUtils.filterServices(getServiceList(namespaceName), labels);
        }

        return services;
    }

    public List<Service> getServicesByRs(String namespaceName, String rsName) {
        ReplicaSet replicaSet = resourceHelper.validateRsShouldExists(namespaceName, rsName);

        List<Service> services = new ArrayList<>();
        Map<String, String> labels = replicaSet.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            services = CommonUtils.filterServices(getServiceList(namespaceName), labels);
        }

        return services;
    }

    public List<Service> getServicesByRc(String namespaceName, String rcName) {
        ReplicationController replicationController = resourceHelper.validateRcShouldExists(namespaceName, rcName);

        List<Service> services = new ArrayList<>();
        Map<String, String> labels = replicationController.getSpec().getTemplate().getMetadata().getLabels();
        if (labels != null) {
            services = CommonUtils.filterServices(getServiceList(namespaceName), labels);
        }

        return services;
    }

    public List<Service> getServicesByLabels(String namespaceName, Map<String, String> labels) {
        List<Service> services = new ArrayList<>();
        if (labels != null) {
            services = CommonUtils.filterServices(getServiceList(namespaceName), labels);
        }

        return services;
    }

    public Service getServiceByStatefulSet(String namespaceName, String statefulSetName) {
        StatefulSet statefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName,statefulSetName);

        String serviceName = statefulSet.getSpec().getServiceName();
        return getService(namespaceName, serviceName);
    }

    public Service update(String namespaceName, Service service) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String serviceName = service.getMetadata().getName();
        Service oldService = resourceHelper.validateServiceShouldExists(namespaceName,serviceName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldService, ResourceType.SERVICE);

        resourceHelper.removeAppLabel(service);

        KubernetesClient client = clientManager.getClient();
        service = client.services().inNamespace(namespaceName).withName(serviceName).replace(service);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.REPLICATION_CONTROLLER)
                .resourceName(serviceName)
                .extras(ImmutableMap.of("originName", serviceName,
                        "currentName", service.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return service;
    }
}
