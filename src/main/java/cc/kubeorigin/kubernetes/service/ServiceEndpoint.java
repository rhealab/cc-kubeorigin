package cc.kubeorigin.kubernetes.service;

import cc.kubeorigin.KubernetesProperties;
import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/services")
@Api(value = "Service", description = "Operation about service.", produces = "application/json")
public class ServiceEndpoint {
    @Inject
    private ServiceManagement serviceManagement;

    @Inject
    private KubernetesProperties properties;

    @GET
    @ApiOperation(value = "List service in namespace.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceList(@PathParam("namespaceName") String namespaceName) {
        List<Service> services = serviceManagement.getServiceList(namespaceName);
        return Response.ok(ServiceDto.from(services, properties.getVipHost())).build();
    }

    @POST
    @ApiOperation(value = "List service belong to given apps in namespace.", responseContainer = "List", response = ServiceDto.class)
    public Response getServicesOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        List<Service> services = serviceManagement.getServicesOfApps(namespaceName, appNames);
        return Response.ok(ServiceDto.from(services, properties.getVipHost())).build();
    }

    @GET
    @Path("origin_list")
    @ApiOperation(value = "List service in namespace, in k8s original form", responseContainer = "List", response = Service.class)
    public Response getServiceListOrigin(@PathParam("namespaceName") String namespaceName) {
        List<Service> services = serviceManagement.getServiceList(namespaceName);
        return Response.ok(services).build();
    }

    @GET
    @Path("{serviceName}")
    @ApiOperation(value = "Get service info.", response = ServiceDto.class)
    public Response getService(@PathParam("namespaceName") String namespaceName,
                               @PathParam("serviceName") String serviceName) {
        Service service = serviceManagement.getService(namespaceName, serviceName);
        if (service != null) {
            return Response.ok(ServiceDto.from(service, properties.getVipHost())).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.SERVICE_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "service", serviceName)).
                    build();
        }
    }

    @GET
    @Path("{serviceName}/origin")
    @ApiOperation(value = "Get service info of k8s original format.", response = Service.class)
    public Response getServiceOrigin(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("serviceName") String serviceName) {
        Service service = serviceManagement.getService(namespaceName, serviceName);
        if (service != null) {
            return Response.ok(service).build();
        } else {
            return new ReturnCodeResponseBuilder().
                    codeName(KubeoriginReturnCodeNameConstants.SERVICE_NOT_FOUND).
                    payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "service", serviceName)).
                    build();
        }
    }

    @DELETE
    @Path("{serviceName}")
    @ApiOperation(value = "Delete service.")
    public Response deleteService(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("serviceName") String serviceName) {
        boolean deleted = serviceManagement.deleteService(namespaceName, serviceName);
        if (deleted) {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.OK)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "service", serviceName))
                    .build();
        } else {
            return new ReturnCodeResponseBuilder()
                    .codeName(KubeoriginReturnCodeNameConstants.SERVICE_NOT_FOUND)
                    .payload(ImmutableMap.of(
                            "namespace", namespaceName,
                            "service", serviceName))
                    .build();
        }
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get services by deployment.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceByDeployment(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("deploymentName") String deploymentName) {
        List<Service> services = serviceManagement.getServicesByDeployment(namespaceName, deploymentName);
        return Response.ok(ServiceDto.from(services, properties.getVipHost())).build();
    }

    @GET
    @Path("by_rc/{rcName}")
    @ApiOperation(value = "Get services by rc.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceByRc(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("rcName") String rcName) {
        List<Service> services = serviceManagement.getServicesByRc(namespaceName, rcName);
        return Response.ok(ServiceDto.from(services, properties.getVipHost())).build();
    }

    @GET
    @Path("by_rs/{rsName}")
    @ApiOperation(value = "Get services by rs.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceByRs(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("rsName") String rsName) {
        List<Service> services = serviceManagement.getServicesByRs(namespaceName, rsName);
        return Response.ok(ServiceDto.from(services, properties.getVipHost())).build();
    }

    @GET
    @Path("by_statefulset/{statefulsetName}")
    @ApiOperation(value = "Get services by statefulset.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("statefulsetName") String statefulsetName) {
        Service service = serviceManagement.getServiceByStatefulSet(namespaceName, statefulsetName);
        List<Service> serviceList = new ArrayList<>();
        if (service != null) {
            serviceList.add(service);
        }
        return Response.ok(ServiceDto.from(serviceList, properties.getVipHost())).build();
    }

    @PUT
    @ApiOperation(value = "Update service by json.", response = ServiceDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           Service service) {
        Service result = serviceManagement.update(namespaceName, service);
        return Response.ok(ServiceDto.from(result, properties.getVipHost())).build();
    }
}
