package cc.kubeorigin.kubernetes.pod;

import cc.kubeorigin.audit.operation.OperationAuditMessage;
import cc.kubeorigin.audit.operation.OperationAuditMessageProducer;
import cc.kubeorigin.audit.operation.OperationType;
import cc.kubeorigin.audit.operation.ResourceType;
import cc.kubeorigin.audit.request.UserContext;
import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.ResourceHelper;
import cc.kubeorigin.kubernetes.utils.KubernetesUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author xzy on 2017/2/23.
 */
@Component
public class PodsManagement {
    private final KubernetesClientManager clientManager;

    private final OperationAuditMessageProducer operationAuditMessageProducer;

    private final ResourceHelper resourceHelper;

    @Inject
    public PodsManagement(KubernetesClientManager clientManager, OperationAuditMessageProducer operationAuditMessageProducer, ResourceHelper resourceHelper) {
        this.clientManager = clientManager;
        this.operationAuditMessageProducer = operationAuditMessageProducer;
        this.resourceHelper = resourceHelper;
    }

    public List<Pod> getPods(String namespaceName) {
        List<Pod> pods = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .list()
                .getItems();
        return Optional.ofNullable(pods).orElseGet(ArrayList::new);
    }

    public List<Pod> getPodsOfApps(String namespaceName, List<String> appNames) {
        KubernetesClient client = clientManager.getClient();
        List<Deployment> deployments = KubernetesUtils.getDeploymentsOfApps(client, namespaceName, appNames);
        List<ReplicaSet> rs = KubernetesUtils.getReplicaSetsOfDeployments(client, namespaceName, deployments);
        return KubernetesUtils.getPodsOfReplicaSet(client, namespaceName, rs);
    }

    public Pod getPod(String namespaceName, String podName) {
        return clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withName(podName)
                .get();
    }

    public void deletePod(String namespaceName, String podName, Long gracePeriodSeconds) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        if (gracePeriodSeconds != null) {
            clientManager.getClient()
                    .pods()
                    .inNamespace(namespaceName)
                    .withName(podName)
                    .withGracePeriod(gracePeriodSeconds)
                    .delete();
        } else {
            clientManager.getClient()
                    .pods()
                    .inNamespace(namespaceName)
                    .withName(podName)
                    .delete();
        }

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.POD)
                .resourceName(podName)
                .build();
        operationAuditMessageProducer.send(message);
    }

    public List<Pod> getPodsByDeployment(String namespaceName, String deploymentName) {
        Deployment deployment = resourceHelper.validateDeploymentShouldExists(namespaceName, deploymentName);

        Map<String, String> podLabels = deployment.getSpec().getTemplate().getMetadata().getLabels();
        return getPodsByLabels(namespaceName, podLabels);
    }

    public List<Pod> getPodsByService(String namespaceName, String serviceName) {
        Service service = resourceHelper.validateServiceShouldExists(namespaceName, serviceName);

        Map<String, String> podLabels = service.getSpec().getSelector();
        return getPodsByLabels(namespaceName, podLabels);
    }

    public List<Pod> getPodsByRc(String namespaceName, String rcName) {
        ReplicationController replicationController = resourceHelper.validateRcShouldExists(namespaceName, rcName);
        Map<String, String> podLabels = replicationController.getSpec().getSelector();
        return getPodsByLabels(namespaceName, podLabels);
    }

    public List<Pod> getPodsByRs(String namespaceName, String rsName) {
        ReplicaSet replicaSet = resourceHelper.validateRsShouldExists(namespaceName, rsName);
        //FIXME can not match Expressions
        Map<String, String> podLabels = replicaSet.getSpec().getSelector().getMatchLabels();
        return getPodsByLabels(namespaceName, podLabels);
    }

    public List<Pod> getPodsByStatefulSet(String namespaceName, String statefulSetName) {
        StatefulSet statefulSet = resourceHelper.validateStatefulsetShouldExists(namespaceName, statefulSetName);

        Map<String, String> podLabelMap = statefulSet.getSpec().getSelector().getMatchLabels();
        return getPodsByLabels(namespaceName, podLabelMap);
    }

    public List<Pod> getPodsByLabels(String namespaceName, Map<String, String> labels) {
        List<Pod> pods = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withLabels(labels)
                .list()
                .getItems();
        return Optional.ofNullable(pods).orElseGet(ArrayList::new);
    }

    public Pod update(String namespaceName, Pod pod) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String podName = pod.getMetadata().getName();
        Pod oldPod = resourceHelper.validatePodShouldExists(namespaceName, podName);

        resourceHelper.validateResourceIsOrigin(namespaceName, oldPod, ResourceType.POD);

        resourceHelper.removeAppLabel(pod);

        pod = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withName(podName)
                .replace(pod);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(UserContext.getUserId())
                .requestId(UserContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.POD)
                .resourceName(podName)
                .extras(ImmutableMap.of("originName", podName,
                        "currentName", pod.getMetadata().getName()))
                .build();
        operationAuditMessageProducer.send(message);

        return pod;
    }

}
