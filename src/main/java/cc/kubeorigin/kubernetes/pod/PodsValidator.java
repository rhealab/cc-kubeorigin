package cc.kubeorigin.kubernetes.pod;

import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.retcode.KubeoriginReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/2/7.
 */
@Component
public class PodsValidator {

    private final KubernetesClientManager clientManager;

    @Inject
    public PodsValidator(KubernetesClientManager clientManager) {
        this.clientManager = clientManager;
    }

    public Pod validatePodShouldExists(String namespaceName, String podName) {
        Pod pod = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withName(podName)
                .get();

        if (pod == null) {
            throw new CcException(KubeoriginReturnCodeNameConstants.POD_NOT_FOUND,
                    ImmutableMap.of("namespace", namespaceName,
                            "pod", podName));
        }

        return pod;
    }
}
