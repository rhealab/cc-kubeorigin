package cc.kubeorigin.kubernetes.pod;

import cc.kubeorigin.kubernetes.pod.dto.PodDto;
import io.fabric8.kubernetes.api.model.Pod;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@RestController
@Produces(MediaType.APPLICATION_JSON)
@Path("origin/namespaces/{namespaceName}/pods")
@Api(value = "Pod", description = "Operation about pods.", produces = "application/json")
public class PodsEndpoint {

    private final PodsManagement management;

    private final PodsValidator validator;

    @Inject
    public PodsEndpoint(PodsManagement management, PodsValidator validator) {
        this.management = management;
        this.validator = validator;
    }

    @GET
    @ApiOperation(value = "List pods in namespace.", responseContainer = "List", response = PodDto.class)
    public Response getPods(@PathParam("namespaceName") String namespaceName) {
        List<Pod> pods = management.getPods(namespaceName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("origin_list")
    @ApiOperation(value = "List pods in namespace, with k8s original form", responseContainer = "List", response = Pod.class)
    public Response getPodsOrigin(@PathParam("namespaceName") String namespaceName) {
        List<Pod> pods = management.getPods(namespaceName);
        return Response.ok(pods).build();
    }

    @GET
    @Path("{podName}")
    @ApiOperation(value = "Get pod info.", response = PodDto.class)
    public Response getPod(@PathParam("namespaceName") String namespaceName,
                           @PathParam("podName") String podName) {
        Pod pod = validator.validatePodShouldExists(namespaceName, podName);
        return Response.ok(PodDto.from(pod)).build();
    }

    @GET
    @Path("{podName}/origin")
    @ApiOperation(value = "Get pod info of k8s original format.", response = Pod.class)
    public Response getPodOrigin(@PathParam("namespaceName") String namespaceName,
                                 @PathParam("podName") String podName) {
        Pod pod = validator.validatePodShouldExists(namespaceName, podName);
        return Response.ok(pod).build();
    }

    @DELETE
    @Path("{podName}")
    @ApiOperation(value = "Delete pod.")
    public Response deletePod(@PathParam("namespaceName") String namespaceName,
                              @PathParam("podName") String podName,
                              @QueryParam("gracePeriodSeconds") Long gracePeriodSeconds) {
        validator.validatePodShouldExists(namespaceName, podName);
        management.deletePod(namespaceName, podName, gracePeriodSeconds);
        return Response.ok().build();
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get pods by deployment.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByDeployment(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("deploymentName") String deploymentName) {
        List<Pod> pods = management.getPodsByDeployment(namespaceName, deploymentName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_service/{serviceName}")
    @ApiOperation(value = "Get pods by service.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByService(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("serviceName") String serviceName) {
        List<Pod> pods = management.getPodsByService(namespaceName, serviceName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_rc/{rcName}")
    @ApiOperation(value = "Get pods by rc.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByRc(@PathParam("namespaceName") String namespaceName,
                                @PathParam("rcName") String rcName) {
        List<Pod> pods = management.getPodsByRc(namespaceName, rcName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_rs/{rsName}")
    @ApiOperation(value = "Get pods by rs.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByRs(@PathParam("namespaceName") String namespaceName,
                                @PathParam("rsName") String rsName) {
        List<Pod> pods = management.getPodsByRs(namespaceName, rsName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_statefulset/{statefulsetName}")
    @ApiOperation(value = "Get pods by statefulset.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                         @PathParam("statefulsetName") String statefulsetName) {
        List<Pod> podList = management.getPodsByStatefulSet(namespaceName, statefulsetName);
        return Response.ok(PodDto.from(podList)).build();
    }

    @PUT
    @ApiOperation(value = "Update pod by json.", response = PodDto.class)
    public Response update(@PathParam("namespaceName") String namespaceName,
                           Pod pod) {
        Pod result = management.update(namespaceName, pod);
        return Response.ok(PodDto.from(result)).build();
    }

    @POST
    @ApiOperation(value = "Get pods by apps.", responseContainer = "List", response = PodDto.class)
    public Response getPodsOfApps(@PathParam("namespaceName") String namespaceName, List<String> appNames) {
        List<Pod> pods = management.getPodsOfApps(namespaceName, appNames);
        return Response.ok(PodDto.from(pods)).build();
    }
}
