package cc.kubeorigin.kubernetes.pod.dto;

import io.fabric8.kubernetes.api.model.RBDVolumeSource;
import io.fabric8.kubernetes.api.model.Volume;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xzy on 2017/2/22.
 */
public class VolumeDto {
    private String name;
    private PersistentVolumeClaimDto persistentVolumeClaim;
    private RbdDto rbd;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersistentVolumeClaimDto getPersistentVolumeClaim() {
        return persistentVolumeClaim;
    }

    public void setPersistentVolumeClaim(PersistentVolumeClaimDto persistentVolumeClaim) {
        this.persistentVolumeClaim = persistentVolumeClaim;
    }

    public RbdDto getRbd() {
        return rbd;
    }

    public void setRbd(RbdDto rbd) {
        this.rbd = rbd;
    }

    @Override
    public String toString() {
        return "VolumeDto{" +
                "name='" + name + '\'' +
                ", persistentVolumeClaim=" + persistentVolumeClaim +
                ", rbd=" + rbd +
                '}';
    }

    public static List<VolumeDto> from(List<Volume> volumes) {
        List<VolumeDto> dtos = new ArrayList<>();

        if (volumes != null) {
            for (Volume volume : volumes) {
                if (volume.getPersistentVolumeClaim() != null) {
                    VolumeDto dto = new VolumeDto();
                    dto.setName(volume.getName());
                    PersistentVolumeClaimDto persistentVolumeClaimDto = new PersistentVolumeClaimDto();
                    persistentVolumeClaimDto.setClaimName(volume.getPersistentVolumeClaim().getClaimName());
                    dto.setPersistentVolumeClaim(persistentVolumeClaimDto);
                    dtos.add(dto);
                }

                if (volume.getRbd() != null) {
                    VolumeDto dto = new VolumeDto();
                    dto.setName(volume.getName());
                    RBDVolumeSource rbd = volume.getRbd();

                    RbdDto rbdDto = new RbdDto();
                    rbdDto.setPool(rbd.getPool());
                    rbdDto.setImage(rbd.getImage());
                    rbdDto.setMonitors(rbd.getMonitors());
                    rbdDto.setFsType(rbd.getFsType());
                    rbdDto.setUser(rbd.getUser());
                    if (rbd.getSecretRef() != null) {
                        rbdDto.setSecretName(rbd.getSecretRef().getName());
                    }

                    rbdDto.setKeyring(rbd.getKeyring());
                    Boolean readOnly = rbd.getReadOnly();
                    rbdDto.setReadOnly(readOnly == null ? false : readOnly);
                    dto.setRbd(rbdDto);
                    dtos.add(dto);
                }
            }
        }

        return dtos;
    }
}
