package cc.kubeorigin.kubernetes.pod.dto;

import cc.kubeorigin.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Created by xzy on 2017/2/22.
 */
public class ContainerDto {
    private String name;
    private String image;
    private String status;
    private Date startTime;
    private List<EnvVar> env;
    private List<ContainerPort> ports;
    private List<VolumeMount> volumeMounts;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<EnvVar> getEnv() {
        return env;
    }

    public void setEnv(List<EnvVar> env) {
        this.env = env;
    }

    public List<VolumeMount> getVolumeMounts() {
        return volumeMounts;
    }

    public void setVolumeMounts(List<VolumeMount> volumeMounts) {
        this.volumeMounts = volumeMounts;
    }

    @Override
    public String toString() {
        return "ContainerDto{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", startTime=" + startTime +
                ", env=" + env +
                '}';
    }

    public static List<ContainerDto> from(List<ContainerStatus> statusList, List<Container> containers) {
        Map<String, ContainerStatus> statusMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(statusList)) {
            for (ContainerStatus status : statusList) {
                statusMap.put(status.getName(), status);
            }
        }

        List<ContainerDto> dtos = new ArrayList<>();
        if (containers != null) {
            for (Container container : containers) {
                ContainerDto dto = new ContainerDto();
                dto.setName(container.getName());
                dto.setImage(container.getImage());
                ContainerStatus status = statusMap.get(container.getName());
                if (status != null && status.getState() != null) {
                    ContainerStateRunning running = status.getState().getRunning();
                    if (running != null) {
                        dto.setStatus("running");
                        dto.setStartTime(DateUtils.parseK8sDate(running.getStartedAt()));
                    }
                    if (status.getState().getWaiting() != null) {
                        dto.setStatus("waiting");
                    }

                    if (status.getState().getTerminated() != null) {
                        dto.setStatus("terminated");
                    }
                }
                dto.setEnv(container.getEnv());
                dto.setPorts(container.getPorts());
                dto.setVolumeMounts(container.getVolumeMounts());
                dtos.add(dto);
            }
        }

        return dtos;
    }

    public List<ContainerPort> getPorts() {
        return ports;
    }

    public void setPorts(List<ContainerPort> ports) {
        this.ports = ports;
    }
}
