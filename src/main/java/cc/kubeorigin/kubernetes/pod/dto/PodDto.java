package cc.kubeorigin.kubernetes.pod.dto;

import cc.kubeorigin.common.utils.DateUtils;
import cc.kubeorigin.kubernetes.pod.PodHelper;
import io.fabric8.kubernetes.api.model.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xzy on 2017/2/22.
 */

public class PodDto {
    private String name;
    private String uid;
    private String status;
    private int restartCount;
    private long age;
    private String podIP;

    private String namespace;
    private Date startTime;
    private Map<String, String> labels;
    private Map<String, String> annotations;

    private String nodeName;

    private List<ContainerDto> containers;
    private List<VolumeDto> volumes;

    private Date createdOn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(int restartCount) {
        this.restartCount = restartCount;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getPodIP() {
        return podIP;
    }

    public void setPodIP(String podIP) {
        this.podIP = podIP;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<ContainerDto> getContainers() {
        return containers;
    }

    public void setContainers(List<ContainerDto> containers) {
        this.containers = containers;
    }

    public List<VolumeDto> getVolumes() {
        return volumes;
    }

    public void setVolumes(List<VolumeDto> volumes) {
        this.volumes = volumes;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "PodDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", status='" + status + '\'' +
                ", restartCount=" + restartCount +
                ", age=" + age +
                ", podIP='" + podIP + '\'' +
                ", namespace='" + namespace + '\'' +
                ", startTime=" + startTime +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", nodeName='" + nodeName + '\'' +
                ", containers=" + containers +
                ", volumes=" + volumes +
                ", createdOn=" + createdOn +
                '}';
    }

    /**
     * @param pods the k8s pods
     * @return composed pod dto list
     */
    public static List<PodDto> from(List<Pod> pods) {
        return Optional.ofNullable(pods)
                .orElseGet(ArrayList::new)
                .stream()
                .map(PodDto::from)
                .collect(Collectors.toList());
    }

    /**
     * @param pod not null
     * @return composed pod list
     */
    public static PodDto from(Pod pod) {
        ObjectMeta metadata = pod.getMetadata();
        PodSpec spec = pod.getSpec();
        PodStatus status = pod.getStatus();

        PodDto dto = new PodDto();
        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());
        dto.setStartTime(DateUtils.parseK8sDate(status.getStartTime()));
        dto.setLabels(metadata.getLabels());
        dto.setAnnotations(metadata.getAnnotations());
        dto.setStatus(PodHelper.getDisplayStatus(pod));
        dto.setNodeName(spec.getNodeName());
        dto.setPodIP(status.getPodIP());
        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        long age = System.currentTimeMillis() - dto.getCreatedOn().getTime();
        dto.setAge(age);

        List<ContainerStatus> containerStatuses = status.getContainerStatuses();
        int totalRestartCount = Optional.ofNullable(containerStatuses)
                .orElseGet(ArrayList::new)
                .stream()
                .mapToInt(ContainerStatus::getRestartCount)
                .sum();
        dto.setRestartCount(totalRestartCount);

        dto.setContainers(ContainerDto.from(containerStatuses, spec.getContainers()));

        dto.setVolumes(VolumeDto.from(spec.getVolumes()));

        return dto;
    }
}
