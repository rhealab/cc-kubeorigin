package cc.kubeorigin;

import cc.kubeorigin.common.utils.OkHttpClientUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/31.
 */
@Component
@ConfigurationProperties(prefix = "kubernetes")
public class KubernetesProperties {
    private String masterUrl;
    private Admin admin;
    private String[] systemNamespaces;
    private boolean loginWithToken;
    private String token;
    private long httpConnectTimeoutMillis;
    private long httpReadTimeoutMillis;
    private long httpWriteTimeoutMillis;
    private String vipHost;

    @PostConstruct
    private void init() {
        OkHttpClientUtils.setConnectTimeoutMillis(getHttpConnectTimeoutMillis());
        OkHttpClientUtils.setReadTimeoutMillis(getHttpReadTimeoutMillis());
        OkHttpClientUtils.setWriteTimeoutMillis(getHttpWriteTimeoutMillis());
    }

    public String getMasterUrl() {
        return masterUrl;
    }

    public void setMasterUrl(String masterUrl) {
        this.masterUrl = masterUrl;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String[] getSystemNamespaces() {
        return systemNamespaces;
    }

    public void setSystemNamespaces(String[] systemNamespaces) {
        this.systemNamespaces = systemNamespaces;
    }

    public boolean isLoginWithToken() {
        return loginWithToken;
    }

    public void setLoginWithToken(boolean loginWithToken) {
        this.loginWithToken = loginWithToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getHttpConnectTimeoutMillis() {
        return httpConnectTimeoutMillis;
    }

    public void setHttpConnectTimeoutMillis(long httpConnectTimeoutMillis) {
        this.httpConnectTimeoutMillis = httpConnectTimeoutMillis;
    }

    public long getHttpReadTimeoutMillis() {
        return httpReadTimeoutMillis;
    }

    public void setHttpReadTimeoutMillis(long httpReadTimeoutMillis) {
        this.httpReadTimeoutMillis = httpReadTimeoutMillis;
    }

    public long getHttpWriteTimeoutMillis() {
        return httpWriteTimeoutMillis;
    }

    public void setHttpWriteTimeoutMillis(long httpWriteTimeoutMillis) {
        this.httpWriteTimeoutMillis = httpWriteTimeoutMillis;
    }

    public String getVipHost() {
        return vipHost;
    }

    public void setVipHost(String vipHost) {
        this.vipHost = vipHost;
    }

    @Override
    public String toString() {
        return "KubernetesProperties{" +
                "masterUrl='" + masterUrl + '\'' +
                ", admin=" + admin +
                ", systemNamespaces=" + Arrays.toString(systemNamespaces) +
                ", loginWithToken=" + loginWithToken +
                ", token='" + token + '\'' +
                ", httpConnectTimeoutMillis=" + httpConnectTimeoutMillis +
                ", httpReadTimeoutMillis=" + httpReadTimeoutMillis +
                ", httpWriteTimeoutMillis=" + httpWriteTimeoutMillis +
                ", vipHost='" + vipHost + '\'' +
                '}';
    }

    public static class Admin {
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "Admin{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
}
