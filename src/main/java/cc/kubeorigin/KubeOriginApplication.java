package cc.kubeorigin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class KubeOriginApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(KubeOriginApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(KubeOriginApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        logger.debug("Starting BackendApplication ...");
    }
}
