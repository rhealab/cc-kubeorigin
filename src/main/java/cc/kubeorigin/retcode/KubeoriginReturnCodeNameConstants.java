package cc.kubeorigin.retcode;

import cc.lib.retcode.config.ReturnCodeNameConstants;


/**
 * @author xzy on 17-8-1.
 */
public class KubeoriginReturnCodeNameConstants extends ReturnCodeNameConstants {
    public static final String KUBERNETES_SERVER_ERROR = "kubernetes_server_error";
    public static final String KUBERNETES_RESOURCE_NOT_EXISTS = "kubernetes_resource_not_exists";
    public static final String KUBERNETES_RESOURCE_ALREADY_EXISTS = "kubernetes_resource_already_exists";
    public static final String KUBERNETES_RESOURCE_FIELD_ERROR = "kubernetes_resource_field_error";


    public final static String DEPLOYMENT_NOT_FOUND = "deployment_not_found";
    public final static String ENDPOINT_NOT_FOUND = "endpoint_not_found";
    public final static String EVENT_NOT_FOUND = "event_not_found";
    public final static String POD_NOT_FOUND = "pod_not_found";
    public final static String REPLICATION_CONTROLLER_NOT_FOUND = "replication_controller_not_found";
    public final static String REPLICA_SET_NOT_FOUND = "replica_set_not_found";
    public final static String SERVICE_NOT_FOUND = "service_not_found";
    public final static String STATEFULSET_NOT_FOUND = "statefulset_not_found";
    public final static String RESOURCE_NOT_SUPPORTED = "resource_not_supported";
    public final static String RESOURCE_IS_NOT_ORIGIN = "resource_is_not_origin";
    public final static String K8S_SERVER_ERROR = "k8s_server_error";

    public static final String CREATE_OKHTTPCLIENT_ERROR = "create_okhttpclient_error";
    public static final String export_csv___failed = "export_csv___failed";
}
