package cc.kubeorigin;

import cc.lib.retcode.ReturnCode;
import cc.lib.retcode.ReturnCodeManager;
import cc.lib.retcode.mapper.RuntimeExceptionMapper;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RuntimeExceptionMapperTest {
    @Inject
    private RuntimeExceptionMapper mapper;


    private Logger logger = LoggerFactory.getLogger(RuntimeExceptionMapperTest.class);

    @Test
    public void testResourceNotFoundException() throws Exception {
        Map<String, ReturnCode> errorMap = ReturnCodeManager.getReturnCodeMap();
        Collection<ReturnCode> ccErrorList = errorMap.values();
        ArrayList<ReturnCode> list = Lists.newArrayList(ccErrorList);
        list.sort(Comparator.comparing(ReturnCode::getCode));

        for (ReturnCode returnCode : list) {
            logger.info("{}", returnCode);
        }
    }
}