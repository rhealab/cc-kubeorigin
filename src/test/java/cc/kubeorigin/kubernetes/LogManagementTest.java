package cc.kubeorigin.kubernetes;

import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.log.LogsManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(LogManagementTest.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private LogsManagement mng;

    @Test
    public void getLogTest() {
        clientManager.getClient().pods().inNamespace("cc-dev").withName("cc-health-2392643339-cjzxl").watchLog(System.out);
        logger.info("LogTest1");
    }

    @Test
    public void getLogTest2() {
        String a = clientManager.getClient().pods().inNamespace("cc-dev").withName("cc-health-2392643339-cjzxl").limitBytes(11).getLog();
        logger.info("LogTest2");
        logger.info(a);
    }

    @Test
    public void getLog() {
//        List<String> a = mng.getPodLogs("cc-dev", "cc-health-2392643339-cjzxl", "cc-account");
//        logger.info("LogTest3");
//        logger.info(Arrays.toString(a.toArray()));
    }

    @Test
    public void getLogLength() {
//        List<String> a = mng.getPodLogs("zzb", "logger-heavy-1595812593-8wzn9", "logger-heavy", 100*1024);
//        logger.info("LogTest4");
//        logger.info(Arrays.toString(a.toArray()));
    }

}
