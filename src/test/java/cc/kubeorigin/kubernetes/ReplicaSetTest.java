package cc.kubeorigin.kubernetes;

import cc.kubeorigin.kubernetes.rs.RsManagement;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.kubeorigin.kubernetes.Constants.TEST_RESOURCE_UPDATE_NS;

/**
 * Created by xzy on 17-7-7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReplicaSetTest {
    @Inject
    private RsManagement rsManagement;

    @Test
    public void updateRs() {
        ReplicaSet rs = rsManagement.getRs(TEST_RESOURCE_UPDATE_NS, "busybox");
        rs.getMetadata().getLabels().put("test", "update");
        rsManagement.update(TEST_RESOURCE_UPDATE_NS, rs);
    }
}
