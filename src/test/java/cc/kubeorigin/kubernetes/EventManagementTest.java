package cc.kubeorigin.kubernetes;

import cc.kubeorigin.kubernetes.event.EventManagement;
import io.fabric8.kubernetes.api.model.Event;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.kubeorigin.kubernetes.Constants.TEST_RESOURCE_UPDATE_NS;

/**
 * Created by yanzhixiang on 17-7-7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EventManagementTest {
    @Inject
    private EventManagement eventManagement;

    @Test
    public void updateEvent() {
        Event event = eventManagement.getEventByName(TEST_RESOURCE_UPDATE_NS, "busybox-xvdhf.14cef29ed6876821");
        event.setMessage("test update event");
        eventManagement.update(TEST_RESOURCE_UPDATE_NS, event);
    }
}
