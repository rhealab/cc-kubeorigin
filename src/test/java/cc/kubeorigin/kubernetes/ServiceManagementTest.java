package cc.kubeorigin.kubernetes;

import cc.kubeorigin.KubernetesProperties;
import cc.kubeorigin.kubernetes.service.ServiceDto;
import cc.kubeorigin.kubernetes.service.ServiceManagement;
import io.fabric8.kubernetes.api.model.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.kubeorigin.kubernetes.Constants.*;
import static org.junit.Assert.assertTrue;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/10.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(ServiceManagementTest.class);

    @Inject
    private KubernetesProperties properties;

    @Inject
    private ServiceManagement serviceManagement;

    @Test
    public void serviceList() {
        List<Service> serviceList = serviceManagement.getServiceList(TEST_NAMESPACE_NAME);
        List<ServiceDto> dtoList = ServiceDto.from(serviceList, properties.getVipHost());
        logger.info(dtoList.toString());
    }

    @Test
    public void getService() {
        List<Service> serviceList = serviceManagement.getServiceList(TEST_NAMESPACE_NAME);
        String name = serviceList.get(0).getMetadata().getName();
        Service service = serviceManagement.getService(TEST_NAMESPACE_NAME, name);
        ServiceDto dto = ServiceDto.from(service, properties.getVipHost());
        logger.info(dto.toString());
    }

    @Test
    public void getServiceByStatefulSetFunction() {
        Service serviceList = serviceManagement.getServiceByStatefulSet(TEST_STATEFULSET_NS_TRUE, TEST_STATEFULSET_NAME_TRUE);
        assertTrue(serviceList != null);
    }

    @Test
    public void updateService() {
        Service service = serviceManagement.getService(TEST_RESOURCE_UPDATE_NS, "busybox");
        service.getMetadata().getLabels().put("test", "update");
        serviceManagement.update(TEST_RESOURCE_UPDATE_NS, service);
    }
}
