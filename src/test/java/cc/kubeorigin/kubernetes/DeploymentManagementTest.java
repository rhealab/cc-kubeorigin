package cc.kubeorigin.kubernetes;

import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.deployment.DeploymentDto;
import cc.kubeorigin.kubernetes.deployment.DeploymentManagement;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.ReplicationController;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.kubeorigin.kubernetes.Constants.TEST_DEPLOYMENT_NAME;
import static cc.kubeorigin.kubernetes.Constants.TEST_NAMESPACE_NAME;
import static cc.kubeorigin.kubernetes.Constants.TEST_RESOURCE_UPDATE_NS;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/13.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeploymentManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentManagementTest.class);

    @Inject
    private DeploymentManagement deploymentManagement;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Test
    public void getDeploymentList() {
        List<Deployment> deploymentList = deploymentManagement.getDeploymentList(TEST_NAMESPACE_NAME);
        List<Pod> pods = podsManagement.getPods(TEST_NAMESPACE_NAME);
        List<DeploymentDto> deploymentDtos = DeploymentDto.from(deploymentList, pods);

        logger.info(deploymentDtos.toString());
    }

    @Test
    public void getDeployment() {
        Deployment deployment = deploymentManagement.getDeployment(TEST_NAMESPACE_NAME, TEST_DEPLOYMENT_NAME);
        List<Pod> pods = deploymentManagement.getDeploymentPods(TEST_NAMESPACE_NAME, deployment);
        DeploymentDto dto = DeploymentDto.from(deployment, pods);

        logger.info(dto.toString());
    }

    @Test
    public void updateDeployment() {
        Deployment deployment = deploymentManagement.getDeployment(TEST_RESOURCE_UPDATE_NS, "busybox");
        deployment.getMetadata().getLabels().put("test", "update");
        deployment.getSpec().getSelector().getMatchLabels().put("test", "update");
        deployment.getSpec().getTemplate().getMetadata().getLabels().put("test", "update");
        deploymentManagement.update(TEST_RESOURCE_UPDATE_NS, deployment);
    }
}
