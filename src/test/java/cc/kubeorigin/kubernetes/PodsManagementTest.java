package cc.kubeorigin.kubernetes;

import cc.kubeorigin.kubernetes.pod.dto.PodDto;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import io.fabric8.kubernetes.api.model.Pod;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.kubeorigin.kubernetes.Constants.TEST_NAMESPACE_NAME;
import static cc.kubeorigin.kubernetes.Constants.TEST_RESOURCE_UPDATE_NS;
import static cc.kubeorigin.kubernetes.Constants.TEST_SERVICE_NAME;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/10.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class PodsManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(ServiceManagementTest.class);

    @Inject
    private PodsManagement podsManagement;

    @Test
    public void getPods() {
        List<Pod> pods = podsManagement.getPods(TEST_NAMESPACE_NAME);
        List<PodDto> podDtos = PodDto.from(pods);
        logger.info(podDtos.toString());
    }

    @Test
    public void getPod() {
        List<Pod> pods = podsManagement.getPods(TEST_NAMESPACE_NAME);
        String name = pods.get(0).getMetadata().getName();
        Pod pod = podsManagement.getPod(TEST_NAMESPACE_NAME, name);
        PodDto podDto = PodDto.from(pod);
        logger.info(podDto.toString());
    }

    @Test
    public void getPodsByService() {
        List<Pod> podList = podsManagement.getPodsByService(TEST_NAMESPACE_NAME, TEST_SERVICE_NAME);
        List<PodDto> dtoList = PodDto.from(podList);
        logger.info(dtoList.toString());
    }

    @Test
    public void updatePod() {
        Pod pod = podsManagement.getPod(TEST_RESOURCE_UPDATE_NS, "busybox-658214511-0b9q6");
        pod.getMetadata().getLabels().put("test", "update");
        podsManagement.update(TEST_RESOURCE_UPDATE_NS, pod);
    }
}
