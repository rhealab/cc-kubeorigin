package cc.kubeorigin.kubernetes;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public interface Constants {
    String TEST_NAMESPACE_NAME = "cc-dev";
    String TEST_DEPLOYMENT_NAME = "cc-mysql";
    String TEST_SERVICE_NAME = "cc-mysql";

    String TEST_STATEFULSET_NS_TRUE = "4tools";
    String TEST_STATEFULSET_NS_FALSE = "cc-zhtang";
    String TEST_STATEFULSET_NAME_TRUE = "ldaps-ha";
    String TEST_STATEFULSET_NAME_FALSE = "false-name-d";
    String TEST_FAULT_NAMESPACE_NAME = "cc-dev-xxx";
    String TEST_FAULT_SERVICE_NAME = "cc-mysql-xxx";

    String TEST_RESOURCE_UPDATE_NS = "cc-yzx";
}
