package cc.kubeorigin.kubernetes;

import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.pod.PodsManagement;
import cc.kubeorigin.kubernetes.statefulset.StatefulSetManagement;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.kubeorigin.kubernetes.Constants.*;

/**
 * @author Created by mrzihan.tang@gmail.com on 05/07/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StatefulSetManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(StatefulSetManagementTest.class);

    @Inject
    private StatefulSetManagement statefulSetManagement;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private KubernetesClientManager clientManager;

    @Test
    public void getStatefulSetListFunction() {
        List<StatefulSet> statefulSetList = statefulSetManagement.getStatefulSetList(TEST_STATEFULSET_NS_TRUE);
        assertEquals(statefulSetList.size(), 1);
        statefulSetList = statefulSetManagement.getStatefulSetList(TEST_STATEFULSET_NS_FALSE);
        assertEquals(statefulSetList.size(), 0);
    }

    @Test
    public void getStatefulSetFunction() {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(TEST_STATEFULSET_NS_TRUE, TEST_STATEFULSET_NAME_TRUE);
        assertTrue(statefulSet != null);
        statefulSet = statefulSetManagement.getStatefulSet(TEST_STATEFULSET_NS_TRUE, TEST_STATEFULSET_NAME_FALSE);
        assertTrue(statefulSet == null);
        statefulSet = statefulSetManagement.getStatefulSet(TEST_STATEFULSET_NS_FALSE, TEST_STATEFULSET_NAME_TRUE);
        assertTrue(statefulSet == null);
    }

    @Test
    public void updateRs() {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(TEST_RESOURCE_UPDATE_NS, "busybox");
        statefulSet.getMetadata().getLabels().put("test", "update");
        statefulSetManagement.update(TEST_RESOURCE_UPDATE_NS, statefulSet);
    }

    @Test
    public void deleteRs(){
        clientManager.getClient().apps().statefulSets().inNamespace("cc-yzx-dev").withName("web").delete();
    }
}
