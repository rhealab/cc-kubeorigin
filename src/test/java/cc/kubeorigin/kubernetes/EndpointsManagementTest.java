package cc.kubeorigin.kubernetes;

import cc.kubeorigin.common.KubernetesClientManager;
import cc.kubeorigin.kubernetes.endpoint.EndpointDto;
import cc.kubeorigin.kubernetes.endpoint.EndpointsManagement;
import io.fabric8.kubernetes.api.model.Endpoints;
import io.fabric8.kubernetes.api.model.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.kubeorigin.kubernetes.Constants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EndpointsManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(EndpointsManagementTest.class);

    @Inject
    private EndpointsManagement endpointsManagement;

    @Test
    public void getEndpoints() {
        List<Endpoints> endpoints = endpointsManagement.getEndpoints(TEST_NAMESPACE_NAME);
        List<EndpointDto> dtos = EndpointDto.from(endpoints);
        logger.info(dtos.toString());
    }

    @Test
    public void getEndpoint() {
        List<Endpoints> endpoints = endpointsManagement.getEndpoints(TEST_NAMESPACE_NAME);
        String name = endpoints.get(0).getMetadata().getName();

        Endpoints endpoint = endpointsManagement.getEndpoint(TEST_NAMESPACE_NAME, name);
        EndpointDto dto = EndpointDto.from(endpoint);

        logger.info(dto.toString());
    }

    @Test
    public void getServiceByStatefulSetFunction() {
        List<Endpoints> endpointsList = endpointsManagement.getEndpointByStatefulSet(TEST_STATEFULSET_NS_TRUE, TEST_STATEFULSET_NAME_TRUE);
        assertTrue(endpointsList.size() > 0);
    }

    @Test
    public void updateEndpoints() {
        Endpoints endpoints = endpointsManagement.getEndpoint("cc-yzx", "busybox");
        endpoints.getMetadata().getLabels().put("test", "update");
        endpointsManagement.update("cc-yzx", endpoints);
    }
}
