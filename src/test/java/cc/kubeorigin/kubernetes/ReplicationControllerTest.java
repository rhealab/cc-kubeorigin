package cc.kubeorigin.kubernetes;

import cc.kubeorigin.kubernetes.rc.RcManagement;
import io.fabric8.kubernetes.api.model.ReplicationController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.kubeorigin.kubernetes.Constants.*;

/**
 * Created by xzy on 17-7-7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReplicationControllerTest {
    @Inject
    private RcManagement rcManagement;

    @Test
    public void updateRc() {
        ReplicationController rc = rcManagement.getRc(TEST_RESOURCE_UPDATE_NS, "busybox");
        rc.getMetadata().getLabels().put("test", "update");
        rcManagement.update(TEST_RESOURCE_UPDATE_NS, rc);
    }
}
