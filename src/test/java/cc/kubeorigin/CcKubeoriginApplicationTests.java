package cc.kubeorigin;

import cc.kubeorigin.common.KubernetesClientManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CcKubeoriginApplicationTests {

	@Test
	public void contextLoads() {
	}

}
